class Solution:
    # @param s, a string
    # @return a string
    def reverseWords(self, s):
        s = s[::-1];
        words = s.split();
        words = map(lambda x:x[::-1], words);
        return ' '.join(words)
        
