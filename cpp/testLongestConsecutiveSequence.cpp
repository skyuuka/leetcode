#include <iostream>
#include <vector>
#include <map>
#include "LeetCodeUtils.h"
using namespace std;
using namespace leetcode;

int longestConsecutive(vector<int> &num) {
    int maxLen = 0;
    map<int, int> count;
    for (int i = 0; i < num.size(); i++) {
        count[num[i]] = 1;
    }
    for (int i = 0; i < num.size(); i++) {
        int k = num[i];
        count.erase(k);
        int high = k + 1;
        int low = k - 1;
        while (count.find(high) != count.end()) {
            count.erase(high);
            high++;
        }
        while (count.find(low) != count.end()) {
            count.erase(low);
            low--;
        }
        maxLen = max(maxLen, high - low - 1);
    }
    return maxLen;
}

int main(int argc, const char *argv[])
{
    {
        int a[] = {100, 4, 200, 1, 3, 2};
        vector<int> num(a, a + 6);
        cout << "Input: " << num << endl;
        cout << "Output: " << longestConsecutive(num) << endl;
    }
    {
        int a[] = {0, 0, 1, -1};
        vector<int> num(a, a + 4);
        cout << "Input: " << num << endl;
        cout << "Output: " << longestConsecutive(num) << endl;
    }
    {
        int a[] = {0};
        vector<int> num(a, a + 1);
        cout << "Input: " << num << endl;
        cout << "Output: " << longestConsecutive(num) << endl;
    }
    {
        int a[] = {1, 3, 5, 2, 4};
        vector<int> num(a, a + 5);
        cout << "Input: " << num << endl;
        cout << "Output: " << longestConsecutive(num) << endl;
    }
    return 0;
}
