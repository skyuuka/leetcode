#include <iostream>
#include <vector>
#include <string>
#include <sstream>
using namespace std;

string countAndSay(int n) {
    string seq = "1";
    for (int i = 1; i < n; i++) {
        string s = "";
        char last = seq[0];
        int count = 0;
        for (int j = 0; j < seq.size(); j++) {
            if (seq[j] == last) {
                count++;
            } else {
                s += to_string(count);
                s += last;
                last = seq[j];
                count = 1;
            }
        }
        s += to_string(count);
        s += last;
        seq = s;
    }
    return seq;
}

int main(int argc, const char *argv[])
{
    for (int n = 5; n < 6; n++) {
        cout << n  << " ==> " << countAndSay(n) << endl;
    }
    return 0;
}
