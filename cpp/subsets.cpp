class Solution {
public:
    vector<vector<int> > subsets(vector<int> &S) {
        vector<vector<int> > result;
        vector<int> output;
        if (S.size() == 0) {
            return result;
        }
        result.push_back(output);
        sort(S.begin(), S.end());
        generateSubset(S, 0, result, output);
        return result;
    }
    
    void generateSubset(
        const vector<int> &S, 
        int start, vector<vector<int> > &result, 
        vector<int> &output) {
        for (int i = start; i < S.size(); i++) {
            output.push_back(S[i]);
            result.push_back(output);
            generateSubset(S, i+1, result, output);
            output.pop_back();
        }
    }
};
