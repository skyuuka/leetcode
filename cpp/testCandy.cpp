#include <iostream>
#include <vector>
#include <numeric>
#include "LeetCodeUtils.h"
using namespace std;
using namespace leetcode;

int candy(vector<int> &ratings) {
    if (ratings.size() == 0) {
        return 0;
    }
    vector<int> count(ratings.size(), 1);
    for (int i = 1; i < ratings.size(); i++) {
        if (ratings[i] > ratings[i - 1]) {
            count[i] = count[i - 1] + 1;
        }
    }
    for (int i = ratings.size() - 1; i >= 1; i--) {
        if (ratings[i - 1] > ratings[i] && count[i - 1] <= count[i]) {
            count[i - 1] = count[i] + 1;
        }
    }
    ratings = count;
    return accumulate(count.begin(), count.end(), 0);
}

int main(int argc, const char *argv[])
{
    {
        int r[] = {2, 3, 1, 3, 4};
        vector<int> ratings(r, r + 5);
        cout << "Input:      "  << ratings << endl;
        cout << "Output: " << candy(ratings) << " | "  << ratings << endl;
    }
    {
        int r[] = {4, 3, 1, 3, 4};
        vector<int> ratings(r, r + 5);
        cout << "Input:      "  << ratings << endl;
        cout << "Output: " << candy(ratings) << " | "  << ratings << endl;
    }
    {
        int r[] = {1, 2, 2, 1, 3, 4};
        vector<int> ratings(r, r + 5);
        cout << "Input:      "  << ratings << endl;
        cout << "Output: " << candy(ratings) << " | "  << ratings << endl;
    }
    return 0;
}
