#ifndef __SEARCHINROTATEDARRAY2_H
#define __SEARCHINROTATEDARRAY2_H

#include <cxxtest/TestSuite.h>
namespace SearchInRotatedArray2Wrapper {
#include "SearchInRotatedArray2.h"
}

class SearchInRotatedArray2: public CxxTest::TestSuite {
    private:
        SearchInRotatedArray2Wrapper::Solution solution;

    public:
        void setUp() {

        }

        void tearDown() {

        }

        void testSearch() {
            {
                int A[] = {4, 5, 6, 7, 0, 1, 2};
                TS_ASSERT_EQUALS(solution.search(A, 7, 9), false);
            }
            {
                int A[] = {4, 5, 6, 7, 0, 1, 2};
                TS_ASSERT_EQUALS(solution.search(A, 7, 3), false);
            }
            {
                int A[] = {4, 5, 6, 7, 0, 1, 2};
                TS_ASSERT_EQUALS(solution.search(A, 7, 0), true);
            }
            {
                int A[] = {4, 5, 6, 7, 0, 1, 2};
                TS_ASSERT_EQUALS(solution.search(A, 7, 4), true);
            }
            {
                int A[] = {1, 3, 5};
                TS_ASSERT_EQUALS(solution.search(A, 3, 2), false);
            }
            { 
                int A[] = {1, 3};
                TS_ASSERT_EQUALS(solution.search(A, 2, 3), true);
            }
        }
};

#endif
