#ifndef __LCA_H
#define __LCA_H

#include <cxxtest/TestSuite.h>
#include "LeetCodeUtils.h"

using namespace leetcode;

namespace LCAWrapper {
#include <cstdlib>
#include "LCA.h"
}


class LCATest: public CxxTest::TestSuite {
    private:
        vector<vector<TreeNode*> > tests;
        LCAWrapper::Solution solution;

    public:
        void setUp() {
            // root, p, q, correct
            vector<TreeNode*> test(4, NULL);
            TreeNode *root = NULL;

            // null
            tests.push_back(test);

            // both nodes in left subtree
            root = convertStringToTree("{4,2,5,1,3,#,6}"); 
            test[0] = root;  
            test[1] = root->left->left;
            test[2] = root->left->right;
            test[3] = root->left;
            tests.push_back(test);

            // both nodes in right subtree
            root = convertStringToTree("{4,2,5,1,3,#,6}"); 
            test[0] = root;  
            test[1] = root->right;
            test[2] = root->right->right;
            test[3] = root->right;
            tests.push_back(test);

            // nodes on both sides
            root = convertStringToTree("{4,2,5,1,3,#,6}"); 
            test[0] = root;  
            test[1] = root->left->right;
            test[2] = root->right->right;
            test[3] = root;
            tests.push_back(test);

            // internal and internal
            root = convertStringToTree("{4,2,5,1,3,#,6}"); 
            test[0] = root;  
            test[1] = root->left;
            test[2] = root->right;
            test[3] = root;
            tests.push_back(test);

            // leaf and leaf
            root = convertStringToTree("{4,2,5,1,3,#,6}"); 
            test[0] = root;  
            test[1] = root->left->left;
            test[2] = root->left->right;
            test[3] = root->left;
            tests.push_back(test);

            // leaf and interal
            root = convertStringToTree("{4,2,5,1,3,#,6}"); 
            test[0] = root;  
            test[1] = root->left->right;
            test[2] = root->right;
            test[3] = root;
            tests.push_back(test);
        }

        void tearDown() {
            for (size_t i = 0; i < tests.size(); i++) {
                    freeTree(tests[i][0]);
                    tests[i].clear();
            }
            tests.clear();
        }

        void testLCABottomUp() 
        {
            TS_TRACE("Starting Lowest Common Ancester test");
            for (size_t i = 0; i < tests.size(); i++) {
                TreeNode *root = tests[i][0];
                TreeNode *p = tests[i][1];
                TreeNode *q = tests[i][2];
                TreeNode *correct = tests[i][3];
                TreeNode *result = solution.LCA(root, p, q);
                TSM_ASSERT("Case #" + to_string(i + 1) + ":", 
                        result == correct
                        );
            }
            TS_TRACE("Finishing Lowest Common Ancester test");
        }

        void testLCATopDown() 
        {
            TS_TRACE("Starting Lowest Common Ancester test");
            for (size_t i = 0; i < tests.size(); i++) {
                TreeNode *root = tests[i][0];
                TreeNode *p = tests[i][1];
                TreeNode *q = tests[i][2];
                TreeNode *correct = tests[i][3];
                TreeNode *result = solution.LCATopDown(root, p, q);
                TSM_ASSERT("Case #" + to_string(i + 1) + ":", 
                        result == correct
                        );
            }
            TS_TRACE("Finishing Lowest Common Ancester test");
        }
};

#endif
