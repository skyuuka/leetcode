class Solution {
    public:
        int removeDuplicates(int A[], int n) {
            if (n == 0) {
                return 0;
            }
            int tail = 0;
            int freq = 1;
            for (int i = 1; i < n; i++) {
                if (A[i] == A[tail]) {
                    freq++;
                    if (freq <= 2)
                        tail++;
                } else {
                    tail++;
                    A[tail] = A[i];
                    freq = 1;
                }

            }
            return tail + 1;
        }
};
