#include <iostream>
#include <string>
using namespace std;


int isNumber(const char* s) {
    int len = strlen(s);
    int e = len - 1;
    int i = 0;
    while (i <= e && isspace(s[i])) {
        i++;
    }
    while (i <= e && isspace(s[e])) {
        e--;
    }
    if (s[i] == '+' || s[i] == '-') {
        i++;
    }
    bool num = false;
    bool dot = false;
    bool exp = false;

    while (i <= e) {
        char c = s[i];
        if (c >= '0' && c <= '9') {
            num = true;
        } else if (c == '.') {
            if (exp || dot) {
                return false;
            }
            dot = true;
        } else if (c == 'e') {
            if (exp || num == false) {
                return false;
            }
            exp = true;
            num = false;
        } else if (c == '+' || c == '-') {
            if (s[i - 1] != 'e') {
                return false;
            }
        } else {
            return false;
        }
        i++;
    }
    return num;
}



int main(int argc, const char *argv[])
{
    char *p;

    p = "";
    cout << p << " -> " << isNumber(p) << endl;
    p = "1";
    cout << p << " -> " << isNumber(p) << endl;
    p = " 12 ";
    cout << p << " -> " << isNumber(p) << endl;
    p = " 1.23 ";
    cout << p << " -> " << isNumber(p) << endl;
    p = " 1e23 ";
    cout << p << " -> " << isNumber(p) << endl;
    p = " 1.e2.3 ";
    cout << p << " -> " << isNumber(p) << endl;
    p = " .1e.3 ";
    cout << p << " -> " << isNumber(p) << endl;
    return 0;
}
