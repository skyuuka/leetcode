#include <vector>
#include <iostream>
#include <ostream>
#include <algorithm>
using namespace std;

struct Interval {
    int start;
    int end;
    Interval() : start(0), end(0) {}
    Interval(int s, int e) : start(s), end(e) {}

    friend ostream& operator<<(ostream& os, const Interval &interval) {
        os << "[" << interval.start << "," << interval.end << "]";
        return os;
    }
};

template <typename T>
ostream& operator<<(ostream& os, const vector<T> &elems) {
    os << "[";
    for (auto it = elems.begin(); it != elems.end(); it++) {
        os << *it << ",";
    }
    if (elems.size() == 0) {
        os << "]";
    } else {
        os << "\b]";
    }
    return os;
}

bool mycompare(const Interval &a, const Interval &b) {
    return a.start < b.start;
}

vector<Interval> merge(vector<Interval> &intervals) {
    vector<Interval> result;
    if (intervals.size() == 0) {
        return result;
    }
    sort(intervals.begin(), intervals.end(), mycompare);
    result.push_back(intervals[0]);
    for (int i = 1; i < intervals.size(); i++) {
        int size = result.size();
        if (intervals[i].start > result[size - 1].end) {
            result.push_back(intervals[i]);
        } else {
            result[size - 1].end = max(result[size - 1].end, intervals[i].end);
        }
    }
    return result;
}

int main(int argc, const char *argv[])
{
    vector<Interval> intervals;
    intervals.push_back(Interval(1, 3));
    intervals.push_back(Interval(3, 6));
    intervals.push_back(Interval(2, 7));
    intervals.push_back(Interval(8, 9));
    cout << "Input: " <<  intervals << endl;
    cout << "Output: " <<  merge(intervals) << endl;
    return 0;
}
