#ifndef __SEARCHINSERTEDPOSITION_H
#define __SEARCHINSERTEDPOSITION_H

#include <cxxtest/TestSuite.h>
namespace SearchInsertPositionWrapper {
#include "SearchInsertPosition.h"
}

class SearchInsertPositionTest : public CxxTest::TestSuite {
    private:
        SearchInsertPositionWrapper::Solution solution;

    public:
        void setUp() {

        }

        void tearDown() {

        }

        void testSearchInsertPosition() {
            {
                int A[] = {1, 3, 5, 6};
                TS_ASSERT_EQUALS(solution.searchInsert(A, 4, 5), 2);
            }
            {
                int A[] = {1, 3, 5, 6};
                TS_ASSERT_EQUALS(solution.searchInsert(A, 4, 2), 1);
            }
            {
                int A[] = {1, 3, 5, 6};
                TS_ASSERT_EQUALS(solution.searchInsert(A, 4, 7), 4);
            }
            {
                int A[] = {1, 3, 5, 6};
                TS_ASSERT_EQUALS(solution.searchInsert(A, 4, 0), 0);
            }
            {
                int A[] = {1};
                TS_ASSERT_EQUALS(solution.searchInsert(A, 1, 0), 0);
            }
            {
                int A[] = {1, 3, 5};
                TS_ASSERT_EQUALS(solution.searchInsert(A, 3, 0), 0);
            }
        }
};

#endif
