/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
    public:
        ListNode *reverseBetween(ListNode *head, int m, int n) {
            if (!head) return NULL;
            if (m < 1 || n < 1 || m <= n) {
                return head;

            }
            ListNode *dummy = new ListNode(0);
            dummy->next = head;
            ListNode *prev = dummy;
            int i;
            for (i = 0; i < m - 1; i++) {
                prev = head;
                head = head->next;

            }
            ListNode *tail = head;
            head = head->next;
            tail->next = NULL;
            for (i = 0; i < n - m; i++) {
                ListNode *curr = head;
                head = head->next;

                curr->next = prev->next;
                prev->next = curr;

            }

            tail->next = head;

            head = dummy->next;
            delete dummy;
            return head;
        }

};
