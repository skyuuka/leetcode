/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {

    public:
        TreeNode* LCA(TreeNode *root, TreeNode *p, TreeNode *q) {
            if (root == NULL || p == NULL || q == NULL) {
                return NULL;
            }
            if (root == p || root == q) {
                return root;
            }

            TreeNode *left = LCA(root->left, p, q);
            TreeNode *right = LCA(root->right, p, q);
            if (left && right) {
                return root;
            }
            return left ? left : right;
        }

        int countMatchPQ(TreeNode *root, TreeNode *p, TreeNode *q) {
            if (!root) {
                return 0;
            }
            int matches = countMatchPQ(root->left, p, q) + countMatchPQ(root->right, p, q);
            if (root == p || root == q) {
                return 1 + matches;
            } else {
                return matches;
            }
        }

        TreeNode* LCATopDown(TreeNode *root, TreeNode *p, TreeNode *q) {
            if (!root || !p || !q) {
                return NULL;
            }
            if (root == p || root == q) {
                return root;
            }
            int totalMatches = countMatchPQ(root->left, p, q);
            if (totalMatches == 1) {
                return root;
            } else if (totalMatches == 2) {
                return LCATopDown(root->left, p, q);
            } else {
                return LCATopDown(root->right, p, q);
            }
        }
};
