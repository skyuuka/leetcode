#include <cmath>
#include <iostream>
using namespace std;

double pow2(double x, int n) {
    bool isPositive = true;
    if (n < 0) {
        isPositive = false;
        n = -n;
    }
    if(n == 0) {
        return 1;
    }
    double v = pow2(x, n >> 1);
    if (n % 2) {
        v = v * v * x;
    } else {
        v = v * v;
    }
    if (isPositive) {
        return v; 
    } else {
        return 1.0 / v; 
    }
}

int main() {
    double x = 1.05;
    for (int n = 0; n < 10; n++) {
        cout << "pow(" << x << "," << n << ") = " << pow2(x, n) << " | " << pow(x, n) << endl;
    }
    return 0;
}
