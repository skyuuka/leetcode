/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
    public:

        ListNode *mergeList(ListNode *head1, ListNode *head2) {
            ListNode *dummy= new ListNode(0);
            ListNode *tail = dummy;
            while (head1 && head2) {
                if (head1->val < head2->val) {
                    tail->next = head1;
                    head1 = head1->next;
                } else {
                    tail->next = head2;
                    head2 = head2->next;
                }
                tail = tail->next;
            }
            if (head1) {
                tail->next = head1;
            } else {
                tail->next = head2;
            }
            tail = dummy->next; // head
            delete dummy;
            return tail;
        } 


        ListNode *findMiddle(ListNode *head) {
            ListNode *slow = head, *fast = head->next;
            while (fast && fast->next) {
                fast = fast->next->next;
                slow = slow->next;
            }
            return slow;
        }

        ListNode *sortList(ListNode *head) {
            if (head == NULL || head->next == NULL) {
                return head;
            }
            ListNode *mid = findMiddle(head);
            //cout << endl << mid->val << endl;
            ListNode *right = sortList(mid->next);
            mid->next = NULL;
            ListNode *left = sortList(head);

            return mergeList(left, right);
        }
};
