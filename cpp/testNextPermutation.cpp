#include <iostream>
#include <vector>
#include <fstream>
using namespace std;

template <typename T>
ostream& operator<< (ostream& os, const vector<T> &elems) {
    os << "[";
    for (int i = 0; i < elems.size(); i++) {
        os << elems[i] << ",";
    }
    if (!elems.empty()) {
        os << "\b";
    }
    os << "]";
    return os;
}

bool next(vector<int> &num, int start) {
    if (num.empty()) {
        return false;
    }
    if (start >= num.size() - 1) { // only 1 number
        return false;
    }
    // at least 2 number
    if (next(num, start + 1)) {
        return true;
    }
    cout << num << endl;
    int i ;
    for (i = start + 1; i < num.size(); i++) {
        if (num[i] > num[start]) {
            swap(num[i], num[start]);
            return true;
        }
    }
    if (i == num.size()) {
        num.push_back(num[start]);
        num.erase(num.begin() + start);
    }
    return false;
}

void nextPermutation(vector<int> &num) {
    next(num, 0);
}


int main() {
    int a[] = {1, 3, 2};
    vector<int> num(a, a + 3);
    cout << "input: " << num << endl;
    nextPermutation(num);
    cout << "output: " << num << endl;
    return 0;
}
