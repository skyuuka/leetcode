#ifndef __RECOVERBST_H
#define __RECOVERBST_H

#include <cxxtest/TestSuite.h>
#include "LeetCodeUtils.h"

using namespace leetcode;

namespace RecoverBSTWrapper {
#include <cstdlib>
#include "RecoverBST.h"
}


class RecoverBSTTest: public CxxTest::TestSuite {
    private:
        vector<pair<TreeNode*, TreeNode*> > tests;
        RecoverBSTWrapper::Solution solution;

    public:
        void setUp() {
            // null
            tests.push_back(
                    make_pair<TreeNode*, TreeNode*>(
                        convertStringToTree("{}"),
                        convertStringToTree("{}")
                        )
                    );

            // single node
            tests.push_back(
                    make_pair<TreeNode*, TreeNode*>(
                        convertStringToTree("{1}"), 
                        convertStringToTree("{1}") 
                        )
                    );

            tests.push_back(
                    make_pair<TreeNode*, TreeNode*>(
                        convertStringToTree("{4,5,2,1,3,#,6}"), 
                        convertStringToTree("{4,2,5,1,3,#,6}") 
                        )
                    );

            // adjacent
            tests.push_back(
                    make_pair<TreeNode*, TreeNode*>(
                        convertStringToTree("{1,2,3}"), 
                        convertStringToTree("{2,1,3}") 
                        )
                    );

            // correct
            tests.push_back(
                    make_pair<TreeNode*, TreeNode*>(
                        convertStringToTree("{1,#,2,#,3}"), 
                        convertStringToTree("{1,#,2,#,3}") 
                        )
                    );

            // start vs end
            tests.push_back(
                    make_pair<TreeNode*, TreeNode*>(
                        convertStringToTree("{4,2,5,6,3,#,1}"), 
                        convertStringToTree("{4,2,5,1,3,#,6}") 
                        )
                    );

            // start vs middle
            tests.push_back(
                    make_pair<TreeNode*, TreeNode*>(
                        convertStringToTree("{4,2,5,3,1,#,6}"), 
                        convertStringToTree("{4,2,5,1,3,#,6}") 
                        )
                    );

            // middle vs end
            tests.push_back(
                    make_pair<TreeNode*, TreeNode*>(
                        convertStringToTree("{4,2,5,1,6,#,3}"), 
                        convertStringToTree("{4,2,5,1,3,#,6}") 
                        )
                    );

        }

        void tearDown() {
            for (size_t i = 0; i < tests.size(); i++) {
                freeTree(tests[i].first);
                freeTree(tests[i].second);
            }
            tests.clear();
        }

        void testRecoverBST() 
        {
            TS_TRACE("Starting recover binary search tree");
            for (size_t i = 0; i < tests.size(); i++) {
                TreeNode *p = tests[i].first;
                TreeNode *q = tests[i].second;
                solution.recoverTree(p);
                TSM_ASSERT("Case #" + to_string(i + 1) + ":", 
                        isSameTree(p, q)
                        );
            }
            TS_TRACE("Finishing recover binary search tree");
        }
};

#endif
