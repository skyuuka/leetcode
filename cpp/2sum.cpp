class Solution {
	public:
        vector<int> twoSum(vector<int> &numbers, int target){
		map<int, int> mapping;
        vector<int> result;
		for (int i = 0; i < numbers.size(); i++) {
			int	searched = numbers[i];
			if (mapping.find(searched) != mapping.end()) {
				result.push_back(mapping[searched] + 1);
				result.push_back(i + 1);
				break;
			} else {
				int	key = target - searched;
                mapping[key] = i;
			}
		}
        return	result;
	}
};
