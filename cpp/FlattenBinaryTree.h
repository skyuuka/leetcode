class Solution {
    private:
        TreeNode *last; // the tail of listed list

    public:
        void flatten(TreeNode *root) {
            last = NULL;
            flattenHelper(root);
        }

        void flattenHelper(TreeNode *root) {
            if (root == NULL) {
                return;
            }

            // add the current node to the tail of linked list
            if (last != NULL) {
                last->right = root;
                last->left = NULL;
            }

            last = root;
            TreeNode *right = root->right;
            // this line will change the root->right, so the above line
            // is added to record the root->right.
            flattenHelper(root->left); 
            flattenHelper(right);
        }
};
