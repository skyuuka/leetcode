#include <iostream>
#include <vector>
#include "LeetCodeUtils.h"
using namespace std;
using namespace leetcode;

int findMin(vector<int> &num) {
    int start = 0;
    int end = num.size() - 1;
    while (start < end) {
        int mid = start + (end - start) / 2;
        if (num[start] < num[end]) {
            end = start;
        } else {
            if (num[start] < num[mid]) {
                start = mid + 1;
            } else {
                end = mid;
            }
        }
    } 
    return num[start];
}

int main(int argc, const char *argv[])
{
    int a[] = {4, 5, 6, 7,  0, 1, 2};
    vector<int> num(a, a + 7);
    cout << "Input: " << num << endl;
    cout << "Output: " << findMin(num) << endl;
    return 0;
}
