#include <iostream>
#include <vector>
#include "../LeetCodeUtils.h"
using namespace std;
using namespace leetcode;

vector<vector<int> > maxLen(3, vector<int>(3));

int dfs(vector<vector<int> > &a, int i, int j) {
    if (a.size() == 0 || a[0].size() == 0) {
        return 0;
    }
    int m = a.size(); 
    int n = a[0].size();

    if (maxLen[i][j] != 0) { 
        return maxLen[i][j];
    } else {
        int len = 1;
        if (i - 1 >= 0 && a[i][j] == a[i - 1][j] + 1) {
            len = max(len, dfs(a, i - 1, j) + 1);
        }
        if (j - 1 >= 0 && a[i][j] == a[i][j - 1] + 1) {
            len = max(len, dfs(a, i, j - 1) + 1);
        }
        if (i + 1 < m && a[i][j] == a[i + 1][j] + 1) {
            len = max(len, dfs(a, i + 1, j) + 1);
        }
        if (j + 1 < n && a[i][j] == a[i][j + 1] + 1) {
            len = max(len, dfs(a, i, j + 1) + 1);
        }
        maxLen[i][j] = len;
        return len;
    }
}

int lengthOfMaxConsecutiveSeq(vector<vector<int> > &a) {
    if (a.size() == 0 || a[0].size() == 0) {
        return 0;
    }
    int rst = 0;
    int m = a.size(); 
    int n = a[0].size();
    for (int i = 0; i < m; i++) {
        for (int j = 0; j < n; j++) {
            rst = max(rst, dfs(a, i, j));
        }
    }
    return rst;
}

int main(int argc, const char *argv[])
{
    //int a_[] = {1, 2, 9, 5, 3, 8, 4, 6, 7};
    int a_[] = {1, 2, 9, 4, 3, 8, 5, 6, 7};
    vector<vector<int> > a;
    a.push_back(vector<int>(a_, a_ + 3));
    a.push_back(vector<int>(a_ + 3, a_ + 6));
    a.push_back(vector<int>(a_ + 6, a_ + 9));
    cout << "Input: " << a << endl;
    cout << "Output " << lengthOfMaxConsecutiveSeq(a) << endl;
    return 0;
}
