#ifndef __SEARCHRANGE_H
#define __SEARCHRANGE_H

#include <cxxtest/TestSuite.h>
#include <vector>
using namespace std;
namespace SearchRangeWrapper {
#include "SearchRange.h"
}

class SearchRange: public CxxTest::TestSuite {
    private:
        SearchRangeWrapper::Solution solution;

    public:
        void setUp() {

        }

        void tearDown() {

        }

        void testSearchRange() {
            {
                int A[] = {5, 7, 7, 8, 8, 10};
                vector<int> result;
                result.push_back(3);
                result.push_back(4);
                TS_ASSERT_EQUALS(solution.searchRange(A, 6, 8), result);
            }
            {
                int A[] = {5, 7, 7, 8, 8, 10};
                vector<int> result (2, -1);
                TS_ASSERT_EQUALS(solution.searchRange(A, 6, 9), result);
            }
            {
                int A[] = {1};
                vector<int> result (2, -1);
                TS_ASSERT_EQUALS(solution.searchRange(A, 1, 0), result);
            }
        }
};

#endif
