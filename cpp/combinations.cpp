class Solution {
public:
    vector<vector<int> > combine(int n, int k) {
        vector<vector<int> > result;
        vector<int> solution;
        generateCombine(n, k, 1, result, solution);
        return result;
    }
    
    void generateCombine(
        int n, 
        int k,
        int start,
        vector<vector<int> > &result,
        vector<int> &solution
        )
    {
        for (int i = start; i <= n; i++) {
            solution.push_back(i);
            if (solution.size() == k) {
                result.push_back(solution);
            }
            
            generateCombine(n, k, i + 1, result, solution);
            solution.pop_back();
        }
    }
};
