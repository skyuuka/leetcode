#include <iostream>
#include <vector>
#include <string>
#include "LeetCodeUtils.h"
using namespace std;
using namespace leetcode;

void search(string s, int start, vector<string> &solution, vector<string> &result) {
    if (solution.size() == 4) {
        if (start == s.size()) { 
            string ip = solution[0];
            for (int i = 1; i < solution.size(); i++) {
                ip += '.';
                ip += solution[i];
            }
            result.push_back(ip);
        }
        return;
    }

    for (int i = start; i < s.size() && i - start + 1 <= 3; i++) {
        // try start to i
        if (i == start || (s[start] != '0' && stoi(s.substr(start, i - start + 1)) <= 255)) {
            solution.push_back(s.substr(start, i - start + 1));
            search(s, i + 1, solution, result);
            solution.pop_back();
        }
    }
}

vector<string> restoreIpAddresses(string s) {
    vector<string> result;
    vector<string> solution;
    search(s, 0, solution, result);
    return result;
}


int main(int argc, const char *argv[])
{
    vector<string> s;
    s.push_back("25525511135");
    s.push_back("1111111111111111111111111111111111111111");
    s.push_back("0000");
    s.push_back("010010");
    for (auto it = s.begin(); it != s.end(); it++) {
        cout << *it << " ==> " << restoreIpAddresses(*it) << endl;
    }
    return 0;
}
