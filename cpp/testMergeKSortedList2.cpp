#include <iostream>
#include <vector>
#include "LeetCodeUtils.h"
using namespace std;
using namespace leetcode;
/**
  Definition for singly-linked list.
  struct ListNode {
  int val;
  ListNode *next;
  ListNode(int x) : val(x), next(NULL) {}
  };
  */
ListNode* merge(ListNode *head1, ListNode *head2) {
    if (head1 == NULL) {
        return head2;
    } else if (head2 == NULL) {
        return head1;
    }
    ListNode *dummy = new ListNode(0);
    ListNode *prev = dummy;
    while (head1 && head2) {
        if (head1->val < head2->val) {
            prev->next = head1;
            head1 = head1->next;
        } else {
            prev->next = head2;
            head2 = head2->next;
        }
        prev = prev->next;
    }
    if (head1) {
        prev->next = head1;
    } else {
        prev->next = head2;
    }
    head1 = dummy->next;
    delete dummy;
    return head1;
}

ListNode* helper(vector<ListNode*> &lists, int l, int r) {
    if (l < r) {
        int m = l + (r - l) / 2;
        return merge(helper(lists, l, m), helper(lists, m + 1, r));
    }
    return lists[l];
}

ListNode* mergeKLists(vector<ListNode*> &lists) {
    if (lists.size() == 0) {
        return NULL;
    }
    return helper(lists, 0, lists.size() - 1);
}

int main(int argc, const char *argv[])
{
    vector<ListNode*> lists;
    for (int i = 0; i < 1000; i++) {
        lists.push_back(new ListNode(5));
        lists.push_back(new ListNode(4));
        lists.push_back(new ListNode(1));
        lists.push_back(new ListNode(2));
        lists.push_back(new ListNode(6));
        lists.push_back(new ListNode(1));
        lists.push_back(new ListNode(3));
        lists.push_back(new ListNode(2));
        lists.push_back(new ListNode(8));
    }
    ListNode *head = mergeKLists(lists);
    cout << head << endl;
    return 0;
}
