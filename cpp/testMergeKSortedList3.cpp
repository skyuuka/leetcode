#include <iostream>
#include <queue>
#include <vector>
#include "LeetCodeUtils.h"
using namespace std;
using namespace leetcode;

class Compare {
    public:
        bool operator() (const ListNode* a, const ListNode* b) const {
            return a->val > b->val;
        }
};

ListNode *mergeKLists(vector<ListNode *> &lists) {
    if (lists.size() == 0) {
        return NULL;
    }
    ListNode *dummy = new ListNode(INT_MIN), *tail = dummy;
    priority_queue<ListNode*, vector<ListNode*>, Compare> heap;
    for (int i = 0; i < lists.size(); i++) {
        if (lists[i]) {
            heap.push(lists[i]);
        }
    }
    while (!heap.empty()) {
        ListNode *top= heap.top();
        heap.pop();
        tail->next = top;
        tail = tail->next;
        if (top->next) {
            heap.push(top->next);
        }
    }
    ListNode *head = dummy->next;
    delete dummy;
    return head;
}

int main(int argc, const char *argv[])
{
    vector<ListNode*> lists;
    for (int i = 0; i < 1000; i++) {
        lists.push_back(new ListNode(5));
        lists.push_back(new ListNode(4));
        lists.push_back(new ListNode(1));
        lists.push_back(new ListNode(2));
        lists.push_back(new ListNode(6));
        lists.push_back(new ListNode(1));
        lists.push_back(new ListNode(3));
        lists.push_back(new ListNode(2));
        lists.push_back(new ListNode(8));
    }
    ListNode *head = mergeKLists(lists);
    cout << head << endl;
    return 0;
}
