#include <iostream>
#include <string>
using namespace std;
/*
 * state: f[i][j] isMatch(s[0 : i], p[0 : j])
 * function: f[i][j] =     
 *                        f[i][j - 1] ||  f[i - 1][j] || f[i - 1][j - 1] if p[j] == '*'
 *                        f[i - 1][j - 1] &&  s[i] == p[j] ||  p[j] == '?' if p[j] != '*'
 * initialize:
 * answer: f[n - 1][m - 1]
 *
 *
 a   b   c   d   e   f   g
 --------------------------------
 *              1   0   0   0   0   0   0   0  
 *          ?   0   1   0   0   0   0   0   0  
 *          b   0   0   1   0   0   0   0   0   
 *          c   0   0   0   1   0   0   0   0   
 *          *   0   0   0   1   1   1   1   1  
 *          f   0   0   0   0   0   0   1   0 
 *          g   0   0   0   0   0   0   0   1
 *
 */
bool isMatch(const char *s, const char *p) {
    if (s == NULL || p == NULL) {
        return false;
    }
    if (strlen(p) == 0) {
        return strlen(s);
    }
    int n = strlen(s);
    int m = strlen(p);
    bool f[n + 1];
    memset(f, 0, sizeof(bool) * (n + 1));
    f[0] = true;
    for (int j = 0; j < m; j++) {
        if (p[j] != '*') {
            for (int i = n - 1; i >= 0; i--) {
                f[i + 1] = f[i] && (s[i] == p[j] || p[j] == '?') ;  
            }
        } else {
            int k = 0;
            while (k <= n && !f[k]) {
                k++;
            }
            while (k <= n) {
                f[k] = true;
                k++;
            }
        }
        f[0] = f[0] && p[j] == '*'; // f[0] = true when all the characters in p are *'s 
    }
    return f[n];
}
int main(int argc, const char *argv[])
{
    char *s, *p;
    s = "aaabababaaabaababbbaaaabbbbbbabbbbabbbabbaabbababab";
    p = "*ab***ba**b*b*aaab*b";
    cout << s << " : " << p << " : " << isMatch(s, p)  << endl;
    s = "aa";
    p = "a";
    cout << s << " : " << p << " : " << isMatch(s, p)  << endl;
    s = "aa";
    p = "aa";
    cout << s << " : " << p << " : " << isMatch(s, p)  << endl;
    s = "aaa";
    p = "aa";
    cout << s << " : " << p << " : " << isMatch(s, p)  << endl;
    s = "aa";
    p = "*";
    cout << s << " : " << p << " : " << isMatch(s, p)  << endl;
    s = "aa";
    p = "a*";
    cout << s << " : " << p << " : " << isMatch(s, p)  << endl;
    s = "ab";
    p = "?*";
    cout << s << " : " << p << " : " << isMatch(s, p)  << endl;
    s = "aab";
    p = "c*a*b";
    cout << s << " : " << p << " : " << isMatch(s, p)  << endl;
    return 0;
}
