class Solution {
    public:
        vector<int> searchRange(int A[], int n, int target) {

            int start, end, mid, low, high;
            vector<int> bound(2);

            // search for left bound
            start = 0;
            end = n - 1;
            while (start + 1 < end) {
                mid = start + (end - start) / 2;
                if (target <= A[mid]) {
                    end = mid;
                } else {
                    start = mid;
                }
            }
            if (A[start] == target) {
                low = start;
            } else if (A[end] == target) {
                low = end;
            } else {
                bound[0] = -1;
                bound[1] = -1;
                return bound;
            }

            // search for right bound
            start = 0;
            end = n - 1;
            while (start + 1 < end) {
                mid = start + (end - start) / 2;
                if (target >= A[mid]) {
                    start = mid;
                } else {
                    end = mid;
                }
            }

            if (A[end] == target) {
                high = end;
            } else if (A[start] == target) {
                high = start;
            } else {
                bound[0] = -1;
                bound[1] = -1;
                return bound;
            }

            bound[0] = low;
            bound[1] = high;
            return bound;
        }

};
