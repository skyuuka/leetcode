class Solution {
public:
    vector<vector<int> > subsetsWithDup(vector<int> &S) 
    {
        vector<vector<int> > result;
        vector<int> output;
        if (S.size() == 0) {
            return result;
        }
        result.push_back(output);
        sort(S.begin(), S.end());
        generateSubsetWithDup(S, 0, result, output);        
    }
    
    void generateSubsetWithDup(
        const vector<int> &S, 
        int start, 
        vector<vector<int> > &result, 
        vector<int> &output)
    {
        for (int i = start; i < S.size(); i++) {
            output.push_back(S[i]);
            result.push_back(output);
            generateSubsetWithDup(S, i + 1, result, output);
            output.pop_back();
            
            while (i + 1 < S.size() && S[i] == S[i + 1]) {
                i++;
            }
        }
    }
};
