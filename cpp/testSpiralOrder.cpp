#include <iostream>
#include <vector>
using namespace std;

template <typename T>
ostream& operator<<(ostream& os, const vector<T> &elems) {
    os << "[";
    for (auto it = elems.begin(); it != elems.end(); it++) {
        os << *it << ",";
    }
    if (elems.size() == 0) {
        os << "]";
    } else {
        os << "\b]";
    }
    return os;
}

vector<int> spiralOrder(vector<vector<int> > &matrix) {
    vector<int> result;
    if (matrix.size() == 0 || matrix[0].size() == 0) {
        return result;
    }
    int m = matrix.size();
    int n = matrix[0].size();
    int row = 0, col = 0;
    while (m > 1 && n > 1) {
        for (int i = 0; i < n; i++) {
            result.push_back(matrix[row][i + col]);
        }
        for (int i = 1; i < m; i++) {
            result.push_back(matrix[i + row][col + n - 1]);
        }
        for (int i = n - 2; i >= 0; i--) {
            result.push_back(matrix[row + m - 1][i + col]);
        }
        for (int i = m - 2; i >= 1; i--) {
            result.push_back(matrix[i + row][col]);
        }
        m = m - 2;
        n = n - 2;
        row++;
        col++;
    }
    if (m == 1 || n == 1) 
    {
        if (m == 1) {
            for (int i = 0; i < n; i++) {
                result.push_back(matrix[row][i + col]);
            }
        } else {
        // n == 1
            for (int i = 0; i < m; i++) {
                result.push_back(matrix[i + row][col]);
            }
        }
    }
    return result;
}

int main(int argc, const char *argv[])
{
    vector<vector<int> > matrix;
    int a1[] = {1, 6};
    int a2[] = {2, 7};
    int a3[] = {3, 8};
    int a4[] = {4, 9};
    int a5[] = {5, 10};
    matrix.push_back(vector<int>(a1, a1 + 2));
    matrix.push_back(vector<int>(a2, a2 + 2));
    matrix.push_back(vector<int>(a3, a3 + 2));
    matrix.push_back(vector<int>(a4, a4 + 2));
    matrix.push_back(vector<int>(a5, a5 + 2));
    cout << "Input: " << matrix << endl;
    vector<int> result = spiralOrder(matrix);
    cout << "Output: " << result << endl;
    return 0;
}
