#include <iostream>
using namespace std;
bool compare(char a, char b) {
    return (a == b || b == '?');
}
bool checkEmpty(const char *p) {
    for (int i = 0; i < strlen(p); i++) {
        if (*(p + i) != '*') {
            return false;
        }
    }
    return true;
}
bool isMatch(const char *s, const char *p) {
    if (s == NULL || p == NULL) {
        return false;
    }
    if (strlen(s) == 0) {
        return checkEmpty(p);
    }
    if (*p == '*') {
        return isMatch(s + 1, p) || isMatch(s, p + 1) || isMatch(s + 1, p + 1);
    } else {
        if (compare(*s, *p)) {
            return isMatch(s + 1, p + 1);
        } else {
            return false;
        }
    }
}
int main(int argc, const char *argv[])
{
    char *s, *p;
    s = "aaabababaaabaababbbaaaabbbbbbabbbbabbbabbaabbababab";
    p = "*ab***ba**b*b*aaab*b";
    cout << s << " : " << p << " : " << isMatch(s, p)  << endl;
    s = "aa";
    p = "a";
    cout << s << " : " << p << " : " << isMatch(s, p)  << endl;
    s = "aa";
    p = "aa";
    cout << s << " : " << p << " : " << isMatch(s, p)  << endl;
    s = "aaa";
    p = "aa";
    cout << s << " : " << p << " : " << isMatch(s, p)  << endl;
    s = "aa";
    p = "*";
    cout << s << " : " << p << " : " << isMatch(s, p)  << endl;
    s = "aa";
    p = "a*";
    cout << s << " : " << p << " : " << isMatch(s, p)  << endl;
    s = "ab";
    p = "?*";
    cout << s << " : " << p << " : " << isMatch(s, p)  << endl;
    s = "aab";
    p = "c*a*b";
    cout << s << " : " << p << " : " << isMatch(s, p)  << endl;
    return 0;
}
