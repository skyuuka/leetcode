#include <iostream>
#include <vector>
#include <string>
using namespace std;
template <typename T>
ostream& operator<<(ostream& os, const vector<T> &elems) {
    os << "[";
    for (auto it = elems.begin(); it != elems.end(); it++) {
        os << *it << ",";
    }
    if (elems.size() == 0) {
        os << "]";
    } else {
        os << "\b]";
    }
    return os;
}
vector<string> fullJustify(vector<string> &words, int L) {
    vector<string> result;
    if (words.size() == 0) {
        return result;
    }
    int len =  0, start = 0; 
    for (int i = 0; i <= words.size(); i++) {
        if (i == words.size() || len + words[i].size() + i - start > L) {
            string line;
            int spaceCount = L - len;
            int spaceSlots = i - 1 - start;
            /*
             * case 1: reach the end
             * case 2: single word in a line
             */
            if (i == words.size() || spaceSlots == 0) {
                for (int j = start; j < i; j++) {
                    line += words[j];
                    if (j != i - 1) {
                        line += " ";
                    }
                }
                line.append(L - line.size(), ' ');
            } else {
                int spaceEach = spaceCount / spaceSlots;
                int spaceExtra = spaceCount % spaceSlots;
                for (int j = start; j < i; j++) {
                    line += words[j];
                    if (j != i - 1) {
                        line.append(spaceEach + (spaceExtra-- > 0 ? 1 : 0), ' ');
                    }
                }
            }
            result.push_back(line);
            start = i;
            len = 0;
        }
        if (i < words.size()) {
            len += words[i].size();
        }
    }
    return result;
}
int main(int argc, const char *argv[])
{
    vector<string> words;
    words.push_back("This");
    words.push_back("is");
    words.push_back("an");
    words.push_back("example");
    words.push_back("of");
    words.push_back("text");
    words.push_back("justification");
    cout << "Input: " << words << endl;
    cout << "Output " << fullJustify(words, 16) << endl;   

    words.clear();
    words.push_back("");
    cout << "Input: " << words << endl;
    cout << "Output " << fullJustify(words, 3) << endl;   

    words.clear();
    words.push_back("a");
    cout << "Input: " << words << endl;
    cout << "Output " << fullJustify(words, 2) << endl;   

    words.clear();
    words.push_back("a");
    words.push_back("b");
    cout << "Input: " << words << endl;
    cout << "Output " << fullJustify(words, 1) << endl;   

    words.clear();
    words.push_back("What");
    words.push_back("must");
    words.push_back("be");
    words.push_back("shall");
    words.push_back("be.");
    cout << "Input: " << words << endl;
    cout << "Output " << fullJustify(words, 12) << endl;   

    words.clear();
    words.push_back("");
    cout << "Input: " << words << endl;
    cout << "Output " << fullJustify(words, 0) << endl;   

    words.clear();
    words.push_back("a");
    cout << "Input: " << words << endl;
    cout << "Output " << fullJustify(words, 1) << endl;   

    return 0;
}
