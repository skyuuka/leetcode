class Solution {
public:
    vector<vector<int> > permute(vector<int> &num) {
        
        vector<vector<int> > result;
        vector<int> visited(num.size(), 0);
        vector<int> solution;
        generatePermute(num, 0, result, solution, visited);
        return result;
    }
    
    void generatePermute(
        vector<int> &num, 
        int start, 
        vector<vector<int> > &result, 
        vector<int> &solution, 
        vector<int> &visited)
    {
        for (int i = 0; i < num.size(); i++) {
            if (visited[i] == 0) {
                solution.push_back(num[i]);
                if (solution.size() == num.size()) {
                    result.push_back(solution);
                }
                visited[i] = 1;
                generatePermute(num, start + 1, result, solution, visited);
                visited[i] = 0;
                solution.pop_back();
            }
        }
    }
};
