#ifndef __LEETCODEUTILSTEST_H
#define __LEETCODEUTILSTEST_H

#include <cxxtest/TestSuite.h>
#include "LeetCodeUtils.h"

using namespace leetcode;

class UtilsTest: public CxxTest::TestSuite {
    private:
        vector<pair<string, TreeNode*> > tests;

 
    public:
        void setUp() {
            tests.push_back(make_pair<string, TreeNode*>("{}", NULL ));

            tests.push_back(make_pair<string, TreeNode*>("{1}", new TreeNode(1)));

            TreeNode *root = new TreeNode(1);
            root->left = new TreeNode(2);
            root->left->right = new TreeNode(3);
            tests.push_back(make_pair<string, TreeNode*>("{1,2,#,#,3}", root));

            root = new TreeNode(1);
            root->right= new TreeNode(2);
            root->right->right = new TreeNode(3);
            root->right->right->right = new TreeNode(4);
            tests.push_back(make_pair<string, TreeNode*>("{1,#,2,#,3,#,4}", root));
            
            root = new TreeNode(1);
            root->left = new TreeNode(2);
            root->right = new TreeNode(3);
            root->right->left = new TreeNode(4);
            root->right->left->right = new TreeNode(5);
            tests.push_back(make_pair<string, TreeNode*>("{1,2,3,#,#,4,#,#,5}", root));

            root = new TreeNode(1);
            root->left = new TreeNode(2);
            root->right = new TreeNode(3);
            root->right->left = new TreeNode(4);
            root->right->right = new TreeNode(5);
            tests.push_back(make_pair<string, TreeNode*>("{1,2,3,#,#,4,5}", root));

            root = new TreeNode(1);
            root->left = new TreeNode(2);
            root->right = new TreeNode(3);
            root->left->left = new TreeNode(4);
            root->left->right =  new TreeNode(5);
            root->right->left = new TreeNode(6);
            root->right->right = new TreeNode(7);
            root->left->left->left = new TreeNode(8);
            root->right->right->right = new TreeNode(9);
            tests.push_back(make_pair<string, TreeNode*>("{1,2,3,4,5,6,7,8,#,#,#,#,#,#,9}", root));
        }

        void tearDown() {
            for (size_t i = 0; i < tests.size(); i++) {
                freeTree(tests[i].second);
            }
            tests.clear();
        }

        void testConstructTree() 
        {
            TS_TRACE("Starting constructing tree from string test");
            for (size_t i = 0; i < tests.size(); i++) {
                string s = tests[i].first;
                TreeNode *p = tests[i].second;
                TreeNode *q = convertStringToTree(s);
                TSM_ASSERT("Case #" + to_string(i + 1) + ":", 
                        isSameTree(p, q));
            }
            TS_TRACE("Finishing constructing tree from string test");
        }

        void testConvertTreeToString() 
        {
            TS_TRACE("Starting converting tree to string test");
            for (size_t i = 0; i < tests.size(); i++) {
                string s = tests[i].first;
                TreeNode *p = tests[i].second;
                string ss = convertTreeToString(p);
                TSM_ASSERT_EQUALS("Case #" + to_string(i + 1) + ":", s, ss);
            }
            TS_TRACE("Finishing converting tree to string test");
        }


};

#endif
