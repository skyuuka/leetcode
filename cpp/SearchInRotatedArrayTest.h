#ifndef __SEARCHINROTATEDARRAY_H
#define __SEARCHINROTATEDARRAY_H

#include <cxxtest/TestSuite.h>
namespace SearchInRotatedArrayWrapper {
#include "SearchInRotatedArray.h"
}

class SearchInRotatedArray: public CxxTest::TestSuite {
    private:
        SearchInRotatedArrayWrapper::Solution solution;

    public:
        void setUp() {

        }

        void tearDown() {

        }

        void testSearch() {
            {
                int A[] = {4, 5, 6, 7, 0, 1, 2};
                TS_ASSERT_EQUALS(solution.search(A, 7, 9), -1);
            }
            {
                int A[] = {4, 5, 6, 7, 0, 1, 2};
                TS_ASSERT_EQUALS(solution.search(A, 7, 3), -1);
            }
            {
                int A[] = {4, 5, 6, 7, 0, 1, 2};
                TS_ASSERT_EQUALS(solution.search(A, 7, 0), 4);
            }
            {
                int A[] = {4, 5, 6, 7, 0, 1, 2};
                TS_ASSERT_EQUALS(solution.search(A, 7, 4), 0);
            }
            {
                int A[] = {1, 3, 5};
                TS_ASSERT_EQUALS(solution.search(A, 3, 2), -1);
            }
        }
};

#endif
