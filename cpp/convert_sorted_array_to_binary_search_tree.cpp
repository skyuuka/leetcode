/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    TreeNode *sortedArrayToBST(vector<int> &num) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        return BuildBST(num, 0, num.size()-1);
    }
    
    TreeNode *BuildBST(vector<int>& num, int start, int end) {
        if (start > end) 
            return NULL;
        int mid = (end+start)/2; 
        TreeNode *root = new TreeNode(num[mid]);
        root->left = BuildBST(num, start, mid-1);
        root->right = BuildBST(num, mid+1, end);
        return root;
    }
};
/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    TreeNode *sortedArrayToBST(vector<int> &num) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        return BuildBST(num, 0, num.size()-1);
    }
    
    TreeNode *BuildBST(vector<int>& num, int start, int end) {
        if (start > end) 
            return NULL;
        int mid = (end+start)/2; 
        TreeNode *root = new TreeNode(num[mid]);
        root->left = BuildBST(num, start, mid-1);
        root->right = BuildBST(num, mid+1, end);
        return root;
    }
};
