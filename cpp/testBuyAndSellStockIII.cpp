#include <iostream>
#include <vector>
#include "LeetCodeUtils.h"
using namespace std;
using namespace leetcode;

int maxProfit(vector<int> &prices) {
    if (prices.size() <= 1) {
        return 0;
    }

    vector<int> left(prices.size());
    vector<int> right(prices.size());

    left[0] = 0;
    int minPrice = prices[0];
    for (int i = 1; i < prices.size(); i++) {
        minPrice = min(minPrice, prices[i]);
        left[i] = max(left[i - 1], prices[i] - minPrice);
    }
    cout << left << endl;

    right[prices.size() - 1] = 0;
    int maxPrice = prices[prices.size() - 1];
    for (int i = prices.size() - 2; i >= 0; i--) {
        maxPrice = max(maxPrice, prices[i]);
        right[i] = max(right[i + 1], maxPrice - prices[i]);
    }
    cout << right << endl;

    int profit = 0;
    for (int i = 0; i < prices.size(); i++) {
        profit = max(left[i] + right[i], profit);
    }
    return profit;
}

int main(int argc, const char *argv[])
{
    int a[] = {1, 3, 2, 4, 5, 1};
    vector<int> prices(a, a + 6);
    cout << "Input: " << prices << endl;
    cout << "Output: " << maxProfit(prices) << endl;
    return 0;
}
