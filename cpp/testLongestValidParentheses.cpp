#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <streambuf>
#include <sstream>
#include <cmath>
using namespace std;
/*
 * state: f[i][j]
 * function: f[i][j] = 
 */
int longestValidParentheses(string s) {
    if (s.empty() || s.size() <= 1) {
        return 0;
    }
    int size = 0;
    int n = s.size();
    vector<vector<bool> > f(n, vector<bool>(n / 2, false));
    int len = 2, l = 1;
    for (int i = 0; i + len - 1 < n; i++) {
        f[i][l] = (s[i] == '(' && s[i + 1] == ')');
    }
    for (len = 4; len <= n; len += 2) {
        l = len / 2;
        for (int i = 0; i + len - 1 < n; i++) {
            f[i][l] = (
                    (f[i][l - 1] && s[i + len - 2] == '(' && s[i + len - 1] == ')') ||
                    (f[i + 1][l - 1] && s[i] == '(' && s[i + len - 1] == ')') ||
                    (f[i + 2][l - 1] && s[i] == '(' && s[i + 1] == ')')
                    );
            if (f[i][l]) {
                size = len;
            }
        }
    }
    return size;
}
int main() {
    ifstream t("longestValidParenthesis.txt");
    stringstream buffer;
    buffer << t.rdbuf();
    string s = buffer.str();
    cout << longestValidParentheses(s) << endl;
    return 0;
}
