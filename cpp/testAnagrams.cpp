#include <iostream>
#include <vector>
#include <ostream>
#include <map>
using namespace std;

template <typename T>
ostream& operator<<(ostream& os, const vector<T> &elems) {
    os << "[";
    for (auto it = elems.begin(); it != elems.end(); it++) {
        os << *it << ",";
    }
    if (elems.size() == 0) {
        os << "]";
    } else {
        os << "\b]";
    }
    return os;
}


vector<string> anagrams(vector<string> &strs) {
    vector<string> result;
    map<vector<int>, vector<string> > container;
    for (size_t i = 0; i < strs.size(); i++) {
        string str = strs[i];
        vector<int> count(26, 0);
        for (int j = 0; j < str.size(); j++) {
            count[str[j] - 'a']++;
        }
        if (container.find(count) == container.end()) {
            container.insert(make_pair(count, vector<string>()));
        }
        container[count].push_back(str);
    }
    for (auto it = container.begin(); it != container.end(); it++) {
        vector<string> &v = it->second;
        if (v.size() > 1) {
            result.insert(result.end(), v.begin(), v.end());
        }
    }
    return result;
}


int main(int argc, const char *argv[])
{
    vector<string> strs;
    strs.push_back("a");
    cout << "Input: " << strs << endl;
    vector<string> result = anagrams(strs);
    cout << "Output: " << result << endl;
    return 0;
}
