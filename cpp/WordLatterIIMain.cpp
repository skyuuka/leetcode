#include <vector>
#include <map>
#include <iostream>
#include <string>
#include <queue>
#include <unordered_set>
#include <ostream>

using namespace std;

ostream& operator<< (ostream& os, const queue<string> &v) {
    queue<string> q(v);
    os << "[";
    while (!q.empty()) {
        os << q.front() << ",";
        q.pop();
    }
    os << "\b]";
    return os;
}

ostream& operator<< (ostream& os, const vector<string> &v) {
    os << "[";
    for(auto it = v.begin(); it != v.end(); it++) {
        os << *it << ",";
    }
    os << "\b]";
    return os;
}

struct Node {
    size_t index;
    string val;
    vector<Node> parents;

    Node(size_t newIndex, string newVal)
        : index(newIndex), val(newVal) {
        parents.clear();
    }

    void addPrev(Node node) {
        parents.push_back(node);
    }

    ~Node() {
        parents.clear();
    }

    friend ostream& operator<< (ostream& os, const Node &node) {
        os << "(" << node.index << "," << node.val << ")";
        return os;
    }
};

void findPath(string start, 
        string end, 
        map<string, Node> &parents,  
        vector<string> &path,  
        vector<vector<string> > &paths 
        ) 
{
    if (parents.find(end) == parents.end()) {
        if (end == start) {
            paths.push_back(path);
        }
        return;
    }

    Node current = parents.at(end);

    for (auto it = current.parents.begin();  it != current.parents.end(); ++it) {
        path.insert(path.begin(), it->val);
        findPath(start, it->val, parents, path, paths);
        path.erase(path.begin());
    }
}

vector<vector<string> > findLadders(string start, string end, unordered_set<string> &dict) {
    vector<vector<string> > result;
    if (dict.size() == 0) {
        return result;
    }

    dict.insert(start);
    dict.insert(end);

    map<string, Node> parents;
    Node current(0, start);
    queue<Node> Q; 
    Q.push(current);
    dict.erase(start);

    bool found = false;

    while(!Q.empty() && !found) {
        size_t size = Q.size();
        for (size_t i = 0; i < size; i++) {
            Node current = Q.front();
            Q.pop();

            for (size_t j = 0; j < current.val.size(); j++) {
                for (char c = 'a'; c <= 'z'; c++) {
                    if (c == current.val[j]) {
                        continue;
                    }

                    Node next(current.index + 1, current.val);
                    next.val[j] = c;

                    if (dict.find(next.val) != dict.end()) {
                        cout << "checking node: " << current << "-->" << next << endl;
                        if (parents.find(next.val) == parents.end()) { // next has NOT been visited before
                            next.addPrev(current);
                            parents.insert(make_pair(next.val, next));
                            Q.push(next);
                            cout << current << "-->" << next << "added!" << endl;
                        } else { // next has been visited before
                            Node& nxt = parents.at(next.val);
                            if (nxt.index == current.index + 1) {
                                nxt.addPrev(current);
                                cout << current << "-->" << nxt << "added!" << endl;
                            }
                        }
                        if (next.val == end) {
                            found = true;  
                        }
                    }

                }

            }
        }
    }

    if (!found) {
        return result;
    }

    cout << endl << "backtracking ..." << endl;

    cout << end << "|";
    for (auto it = parents.begin(); it != parents.end(); it++) {
        Node node = it->second;
        for (auto j = node.parents.begin(); j != node.parents.end(); j++) {
            cout << *j << ",";
        }
        cout << "\b" << endl;
    }


    vector<string> path(1, end);
    findPath(start, end, parents, path, result );
    return result;
}



int main()
{
    //unordered_set<string> dict({"hot", "dot", "dog", "lot", "log"});
    //string start = "hit";
    //string end = "cog";
    unordered_set<string> dict({"a", "b", "c"});
    string start = "a";
    string end = "c";
    cout << "start ..." << endl;
    vector<vector<string> > ladders = findLadders(start, end, dict);
    cout << "done!" << endl;

    cout << "========= Answer ========" << endl;
    cout << "[" << endl;
    for (size_t i = 0; i < ladders.size(); i++) {
        cout << "[";
        for (auto it = ladders[i].begin(); it != ladders[i].end(); ++it) {
            cout << *it << ",";
        }
        cout << "\b]" << endl;
    }
    cout << "]" << endl;
    return 0;
}
