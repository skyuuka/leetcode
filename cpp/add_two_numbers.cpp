/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode *addTwoNumbers(ListNode *l1, ListNode *l2) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int carry = 0;
        ListNode *res = new ListNode(0);
        ListNode *cur = res;
        while (true) {
            int v1 = 0, v2 = 0;
            if (l1) v1 = l1->val;
            if (l2) v2 = l2->val;
            int sum = carry + v1 + v2;
            carry = sum > 9? 1:0;
            cur->val = sum % 10;
           
            if (l1) l1 = l1->next;
            if (l2) l2 = l2->next;
            
                break;
            } 
            cur->next = new ListNode(0);
            cur = cur->next;
        }
        
        if (carry)
            cur->next = new ListNode(carry);
        return res;
    }
};
