#ifndef __SUMROOTTOLEAF_H
#define __SUMROOTTOLEAF_H

#include <cxxtest/TestSuite.h>
#include "LeetCodeUtils.h"

using namespace leetcode;

namespace SumRoot2LeafWrapper {
#include <cstdlib>
#include "SumRoot2Leaf.h"
}


class SumRoot2LeafTest: public CxxTest::TestSuite {
    private:
        SumRoot2LeafWrapper::Solution solution;
        vector<pair<TreeNode*, int> > tests;

    public:
        void setUp() {
            TreeNode *root;
            /*
            // 1
            root = new TreeNode(9);
            tests.push_back(make_pair(root, 9));

            // 2
            root = new TreeNode(1);
            root->left = new TreeNode(2);
            root->right = new TreeNode(3);
            tests.push_back(make_pair(root, 25));
            */

            // 3
            root = new TreeNode(0);
            root->left = new TreeNode(1);
            root->right = new TreeNode(3);
            tests.push_back(make_pair(root, 4));

        }

        void tearDown() {
            for (size_t i = 0; i < tests.size(); i++) {
                freeTree(tests[i].first);
            }
            tests.clear();
        }

        void testSumRoot2Leaf() 
        {
            for (size_t i = 0; i < tests.size(); i++) {
                TreeNode *root = tests[i].first;
                int result = tests[i].second;
                TSM_ASSERT_EQUALS("Test cass #" + to_string(i), 
                        solution.sumNumbers(root), 
                        result);
            }
            /*
            for (size_t i = 0; i < result.size(); i++) {
                vector<string> const& one = result[i];
                cout << "~-----------------" << endl;
                cout << "solution " << i << endl;
                cout << "^-----------------" << endl;
                for (int j = 0; j < one.size(); j++) {
                    cout << one[j] << endl;
                }
            }*/
        }
};

#endif
