class Solution {
    public:
        string addBinary(string a, string b) {
            // Start typing your C/C++ solution below
            // DO NOT write int main() function
            string res;
            int carry = 0;
            int i = a.size()-1, j = b.size()-1;
            while( i>=0 || j>=0) {
                int aa = 0, bb = 0;
                if (i>=0) aa = a[i]-'0';
                if (j>=0) bb = b[j]-'0';
                int sum = carry + aa + bb;
                carry = sum >= 2? 1 : 0;
                res.push_back(char(sum%2 + '0'));
                i--;
                j--;
            }
            if (carry==1)
                res.push_back('1');
            reverse(res.begin(),res.end());
            return res;
        }
};
