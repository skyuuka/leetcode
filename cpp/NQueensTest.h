#ifndef __NQUEENSTEST_H
#define __NQUEENSTEST_H

#include <cxxtest/TestSuite.h>
#include "LeetCodeUtils.h"

using namespace leetcode;

namespace NQueensWrapper {
#include <cstdlib>
#include "NQueens.h"
}


class NQueensTest: public CxxTest::TestSuite {
    private:
        NQueensWrapper::Solution solution;

        vector<pair<int, int> > tests;

    public:
        void setUp() {
            tests.push_back(make_pair(1, 1));
            tests.push_back(make_pair(2, 0));
            tests.push_back(make_pair(3, 0));
            tests.push_back(make_pair(4, 2));
            tests.push_back(make_pair(5, 10));
            tests.push_back(make_pair(6, 4));
            tests.push_back(make_pair(7, 40));
            tests.push_back(make_pair(8, 92));
            tests.push_back(make_pair(9, 352));
            //tests.push_back(make_pair(10, 724));
            //tests.push_back(make_pair(11, 2680));
            //tests.push_back(make_pair(12, 14200));
            //tests.push_back(make_pair(13, 73712));
        }

        void tearDown() {
            tests.clear();
        }

        void testNQueens() 
        {
            for (size_t i = 0; i < tests.size(); i++) {
                int n = tests[i].first;
                int m = tests[i].second;
                vector<vector<string> > result = solution.solveNQueens(n);
                TSM_ASSERT_EQUALS("Test cass #" + to_string(i) + " with input " + to_string(n), 
                    m, result.size());
            }
            /*
            for (size_t i = 0; i < result.size(); i++) {
                vector<string> const& one = result[i];
                cout << "~-----------------" << endl;
                cout << "solution " << i << endl;
                cout << "^-----------------" << endl;
                for (int j = 0; j < one.size(); j++) {
                    cout << one[j] << endl;
                }
            }*/
        }
};

#endif
