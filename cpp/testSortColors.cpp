#include <iostream>
#include <vector>
using namespace std;

template <typename T>
ostream& operator<<(ostream& os, const vector<T> &elems) {
    os << "[";
    for (auto it = elems.begin(); it != elems.end(); it++) {
        os << *it << ",";
    }
    if (elems.size() == 0) {
        os << "]";
    } else {
        os << "\b]";
    }
    return os;
}
void sortColors(int A[], int n) {
    int start = 0, end = n - 1;
    while (start <= end && A[start] == 0) start++;
    while (start <= end && A[end] == 2) end--;
    for (int i = start; i <= end; i++) {
        while (A[i] != 1 && start <= end) {
            if (A[i] == 0) {
                swap(A[start], A[i]);
                while (A[start] == 0) start++;
                if (i < start) i = start;
            } else if (A[i] == 2) {
                swap(A[i], A[end]);
                while (A[end] == 2) end--;
            }
        }
    }
}

int main(int argc, const char *argv[])
{
    {
        int A[] = {1, 2};
        cout << "Input: " << vector<int>(A, A + 2) << endl;
        sortColors(A, 2);
        cout << "Output: " << vector<int>(A, A + 2) << endl;
    }
    {
        int A[] = {2, 1, 1, 0};
        cout << "Input: " << vector<int>(A, A + 4) << endl;
        sortColors(A, 4);
        cout << "Output: " << vector<int>(A, A + 4) << endl;
    }
    {
        int A[] = {0, 2, 1, 2, 0, 2, 0, 1};
        cout << "Input: " << vector<int>(A, A + 8) << endl;
        sortColors(A, 8);
        cout << "Output: " << vector<int>(A, A + 8) << endl;
    }
    return 0;
}
