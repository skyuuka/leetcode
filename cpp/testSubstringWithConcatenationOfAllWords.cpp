#include <iostream>
#include <vector>
#include <unordered_map>
#include "LeetCodeUtils.h"
using namespace std;
using namespace leetcode;

vector<int> findSubstring(string S, vector<string> &L) {
    vector<int> result;
    if (L.size() == 0) {
        return result;
    }
    unordered_map<string, int> lCounter, foundCounter;
    int m = L.size(), n = L[0].size();
    for (int i = 0; i < L.size(); i++) {
        if (lCounter.find(L[i]) == lCounter.end()) {
            lCounter[L[i]] = 1;
        } else {
            lCounter[L[i]]++;
        }
    }

    for (int i = 0; i <= S.size() - m * n; i++) {
        foundCounter.clear();
        int j;
        for (j = 0; j < m; j++) {
            int k = i + j * n;
            string sub = S.substr(k, n);
            if (lCounter.find(sub) == lCounter.end()) {
                break; 
            }
            if (foundCounter.find(sub) == foundCounter.end()) {
                foundCounter[sub] = 1;
            } else {
                foundCounter[sub]++;
            }
            if (foundCounter[sub] > lCounter[sub]) {
                break;
            }
        }
        if (j == m) {
            result.push_back(i);
        }
    }
    return result;
}

int main(int argc, const char *argv[])
{
    string S = "abababababababababababababababababababababababababababa";
    vector<string> T;
    T.push_back("ab");
    T.push_back("ab");
    T.push_back("ab");
    T.push_back("ab");
    T.push_back("ab");
    T.push_back("ab");
    T.push_back("ab");
    cout << "S: " << S << endl;
    cout << "S: " << T << endl;
    cout << findSubstring(S, T) << endl;
    return 0;
}
