class Solution {
public:
    int searchInsert(int A[], int n, int target) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int left = 0;
        int right = n-1;
        
        while (left < right) {
            int middle = left + (right-left)/2;
            if (target == A[middle]) 
                return middle;
            else if (target > A[middle]) {
                if (left == middle)
                    left++;
                else
                    left = middle;
            } else {
                if (right == middle)
                    right--;
                else
                    right = middle;
            }
        }
        return target > A[left] ? left+1:left;
    }
};
