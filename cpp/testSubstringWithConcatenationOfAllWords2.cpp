#include <iostream>
#include <vector>
#include <unordered_map>
#include "LeetCodeUtils.h"
using namespace std;
using namespace leetcode;

vector<int> findSubstring(string S, vector<string> &L) {
    vector<int> result;
    if (L.size() == 0) {
        return result;
    }
    unordered_map<string, int> lCounter, foundCounter;
    int m = L.size(), n = L[0].size();
    for (int i = 0; i < L.size(); i++) {
        if (lCounter.find(L[i]) == lCounter.end()) {
            lCounter[L[i]] = 1;
        } else {
            lCounter[L[i]]++;
        }
    }

    for (int i = 0; i < n; i++) {
        int start = i;
        int numFound = 0;
        foundCounter.clear();

        for (int end = i; end + n - 1 < S.size(); end += n){
            string word = S.substr(end, n);
            if (lCounter.find(word) == lCounter.end()) {
                // word not in L
                start = end + n;
                foundCounter.clear();
                numFound = 0;
            } else {
                if (foundCounter.find(word) == foundCounter.end()) {
                    foundCounter[word] = 1;
                } else {
                    foundCounter[word]++;
                }
                if (foundCounter[word] <= lCounter[word]) {
                    numFound++;
                } else {
                    while (S.substr(start, n) != word) {
                        foundCounter[S.substr(start, n)]--;
                        numFound--;
                        start += n;
                    }
                    foundCounter[S.substr(start, n)]--;
                    start += n;
                }
                if (numFound == m) {
                    result.push_back(start);
                }
            }
        }
    }
    return result;
}

int main(int argc, const char *argv[])
{
    {
        string S = "abababababababababababababababababababababababababababa";
        vector<string> L;
        L.push_back("ab");
        L.push_back("ab");
        L.push_back("ab");
        L.push_back("ab");
        L.push_back("ab");
        L.push_back("ab");
        L.push_back("ab");
        cout << "S: " << S << endl;
        cout << "L: " << L << endl;
        cout << findSubstring(S, L) << endl;
    }
    {
        string S = "abababab";
        vector<string> L;
        L.push_back("a");
        L.push_back("b");
        cout << "S: " << S << endl;
        cout << "L: " << L << endl;
        cout << findSubstring(S, L) << endl;
    }
    {
        string S = "abababab";
        vector<string> L;
        L.push_back("a");
        L.push_back("b");
        L.push_back("a");
        cout << "S: " << S << endl;
        cout << "L: " << L << endl;
        cout << findSubstring(S, L) << endl;
    }
    return 0;
}
