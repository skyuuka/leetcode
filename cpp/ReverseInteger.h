class Solution {
    public:
        int reverse(int x) {
            // Start typing your C/C++ solution below
            // DO NOT write int main() function
            int sign;
            if (x >=0) {
                sign = 1;
            } else {
                sign = -1;
                x = -x;
            }
            int y = 0;
            while (x > 0) {
                y = 10 * y + x%10;
                x = x/10;
            }

            return sign*y;
        }
};
