#include <iostream>
#include <queue>
#include <vector>
using namespace std;
struct Board {
    vector<vector<char> > &board;
    int cols;
    int rows;
    Board(vector<vector<char> > &board) : board(board) {
        rows = board.size();
        cols = rows > 0 ? board[0].size() : 0;
    }
};
void safePush(queue<int> &Q, int r, int c, Board const& board, char val = 'O') {
    if (r >= 0 && r < board.rows && c >= 0 && c < board.cols && board.board[r][c] == val) {
        int index = board.cols * r + c;
        Q.push(index); 
    }
}
void solve(vector<vector<char> > &board) {
    if (board.size() == 0 || board[0].size() == 0) {
        return;
    }
    queue<int> Q;
    Board b(board);
    int rows = b.rows;
    int cols = b.cols;
    for (int r = 0; r < rows; r++) {
        safePush(Q, r, 0, b, 'O');
        safePush(Q, r, cols - 1, b, 'O');
    }
    for (int c = 0; c < cols; c++) {
        safePush(Q, 0, c, b, 'O');
        safePush(Q, rows - 1, c, b, 'O');
    }
    while (!Q.empty()) {
        int index = Q.front();
        Q.pop();
        int r = index / cols;
        int c = index % cols;
        if (board[r][c] == 'O') {
            board[r][c] = 'Y';
        }
        safePush(Q, r - 1, c, b, 'O');
        safePush(Q, r + 1, c, b, 'O');
        safePush(Q, r, c - 1, b, 'O');
        safePush(Q, r, c + 1, b, 'O');
    }
    for (int r = 0; r < rows; r++) {
        for (int c = 0; c < cols; c++) {
            if (board[r][c] == 'O') {
                board[r][c] = 'X';
            } else if (board[r][c] == 'Y') {
                board[r][c] = 'O';
            }
        }
    }
}
void printBoard(vector<vector<char> > const& board) {
    for (int i = 0; i < board.size(); i++) {
        for (int j = 0; j < board[i].size(); j++) {
            cout << board[i][j] << " ";
        }
        cout <<"\b" << endl;
    }
}
int main() {
    {
        vector<vector<char> > board(3, vector<char>(3, 'X'));
        board[1][1] = 'O';
        cout << "old:" << endl;
        printBoard(board);
        solve(board);
        cout << "new:" << endl;
        printBoard(board);
    }
    {
        vector<vector<char> > board(4, vector<char>(4, 'X'));
        board[1][1] = 'O';
        board[1][2] = 'O';
        board[2][2] = 'O';
        board[3][1] = 'O';
        cout << "old:" << endl;
        printBoard(board);
        solve(board);
        cout << "new:" << endl;
        printBoard(board);
    }
}
