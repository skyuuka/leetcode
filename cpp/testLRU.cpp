#include <cstdlib>
#include <vector>
#include <iostream>
#include <map>
#include "LeetCodeUtils.h"
using namespace leetcode;
using namespace std;
class LRUCache{
    private:
        struct Node {
            int key;
            int value;
            Node *prev;
            Node *next;
            Node() : key(-1), value(-1), prev(NULL), next(NULL) {}
            Node(int newKey, int newValue) : key(newKey), value(newValue), prev(NULL), next(NULL) {}
            Node(const Node& other) : key(other.key), value(other.value), prev(NULL), next(NULL) {}
            ~Node() {
                prev = next = NULL;
            }
        };
        int capacity;
        map<int, Node*> cache;
        Node *head;
        Node *tail;
    public:
        LRUCache(int capacity) {
            this->capacity = capacity;
            head = new Node(-1, -1);
            tail = new Node(-1, -1);
            head->next = tail;
            tail->prev = head;
        }
        ~LRUCache() {
            capacity = 0;
            while (head) {
                Node *next = head->next;
                delete head;
                head = next;
            }
        }
        int get(int key) {
            if (cache.find(key) == cache.end()) {
                return -1;
            } 
            Node *current = cache[key];
            // remove current
            remove(current);
            // add to tail
            append(current);
            return current->value;
        }
        void set(int key, int value) {
            Node *current = NULL;
            if (get(key) != -1) {
                current = cache[key];
                current->value = value;
                remove(current);
            } else {
                if (cache.size() == capacity) {
                    // remove head
                    current = head->next;
                    remove(current);
                    cache.erase(current->key);
                }
                if (current == NULL) {
                    current = new Node(key, value);
                } else {
                    current->key = key;
                    current->value = value;
                }
            }
            // add to tail
            append(current);
            cache[key] = current;
        }

        void remove(Node *node) {
            node->prev->next = node->next;
            node->next->prev = node->prev;
        }

        void append(Node *node) {
            tail->prev->next = node;
            node->prev = tail->prev;
            node->next = tail;
            tail->prev = node;
        }
};
int main(int argc, const char *argv[])
{
    {
        LRUCache cache(1);
        vector<int> result;
        cache.set(2, 1); // (2, 1)
        result.push_back(cache.get(2)); // 1
        cache.set(3, 2);  // (3, 2)
        result.push_back(cache.get(2)); // -1
        result.push_back(cache.get(3)); // 2
        cout << result << endl;  // 1, -1, 2
    }
    {
        LRUCache cache(2);
        vector<int> result;
        cache.set(2, 1);  // (2, 1)
        cache.set(1, 1);  // (2, 1)->(1, 1)
        cache.set(2, 3);  // (1, 1)->(2, 3)
        cache.set(4, 1);  // (2, 3)->(4, 1)
        result.push_back(cache.get(1));
        result.push_back(cache.get(2));
        cout << result << endl; // -1, 3
    }
    return 0;
}
