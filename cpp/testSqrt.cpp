#include <iostream>
using namespace std;

int sqrt(int x) {
    int start = 0, end = x;
    while (start + 1 < end) {
        long mid = start + (end - start) / 2;
        if (mid * mid == x) {
            return mid;
        } 
        if (mid * mid < x) {
            start = mid;
        } else {
            end = mid;
        }
    }
    if (end * end <= x) {
        return end;
    }
    return start;
}


int main(int argc, const char *argv[])
{
    int i;
    for (i = 0; i < 100; i++) {
        cout << "sqrt(" << i << ") = " << sqrt(i) << endl;
    }
    i = 2147395599;
    cout << "sqrt(" << i << ") = " << sqrt(i) << endl;
    return 0;
}
