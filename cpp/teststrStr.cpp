#include <iostream>
using namespace std;

char *strStr(char *haystack, char *needle) {
    if (haystack == NULL || needle == NULL) {
        return NULL;
    }
    size_t n = strlen(haystack), len = strlen(needle);
    int i, j;
    for (i = 0; i <= n - len; i++) {
        for (j = 0; j < len; j++) {
            if (*(haystack + i + j) != *(needle + j)) {
                break;
            }
        }
        if (j == len) {
            return haystack + i;
        }
    }
    return NULL;
}

int main() {
    strStr("mississippi", "a");
    return 0;
}
