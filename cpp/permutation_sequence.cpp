#include <iostream>
#include <string>
#include <vector>

using namespace std;

void generatePermutation(
        const int n, 
        const int k,
        int start, 
        string &result,
        string &solution,
        vector<int> &visited, 
        int &count
        )
{
    for (int i = 1; i <= n; i++) {
        if (visited[i-1] == 0) {
            solution.push_back(i + '0');

            cout << solution << endl;
            if (solution.length() == n) {
                count++;
                if (count == k) {
                    result = solution;
                    return;
                }
            }

            visited[i-1] = 1;
            generatePermutation(n, k, start + 1, result, solution, visited, count);
            visited[i-1] = 0;

            solution.pop_back();
        }
    }
}

string getPermutation(int n, int k) 
{
    string result, solution;
    int count = 0;
    vector<int> visited(n,0);
    generatePermutation(n, k, 1, result, solution, visited, count);
    return result;
}


int main()
{
    string result = getPermutation(2, 1);
    cout << "solution is: ";
    cout << result;
    cout << endl;
    return 0;
}
