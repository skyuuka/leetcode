class Solution {
    public:

        const string rom[4][10] = { {"", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"},
            {"", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"},
            {"", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"},
            {"", "M", "MM", "MMM"} };
        string intToRoman(int num) {
            Start typing your C/C++ solution below
                // DO NOT write int main() function   
                string res;
            int i=3;
            while (num > 0) {
                int d = pow(10,i);
                int j = (int)(num/d);
                res += rom[i][j];
                num = num - d*j;
                i--;
            }
            return res;
        }
};
