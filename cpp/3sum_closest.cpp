class Solution
{
public:
    int threeSumClosest(vector<int> &num, int target)
    {
        int res = INT_MAX;
        sort(num.begin(), num.end());
        for (size_t i = 0; i < num.size(); i++)
        {
            int l = i + 1;
            int r = num.size() - 1;
            while (l < r)
            {
                int sum = num[i] + num[l] + num[r];
                if (res == INT_MAX || abs(sum - target) < abs(res - target))
                    res = sum;
                if (sum == target) 
                    return res;
                else if (sum<target) 
                    l++;
                else
                    r--;
            }
        }
        return res;
    }
};



