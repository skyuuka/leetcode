/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
    public:
        int sumNumbers(TreeNode *root) {
            return dfs(root, 0);
        }

        int dfs(TreeNode *root, int prev) {
            if (root == NULL) {
                return 0;
            }

            int sum = prev * 10 + root->val;
            if (root->left == NULL && root->right == NULL) {
                return sum;
            }

            return dfs(root->left, sum) + dfs(root->right, sum);
        }

        int sumNumbers2(TreeNode *root) {
            if (root == NULL) {
                return 0;
            }
            int sum = 0;
            vector<TreeNode*> Q;
            map<TreeNode*, bool> visited;
            Q.push_back(root);
            // post order
            while (!Q.empty()) {
                TreeNode *node = Q.back();
                if (node->left && visited.find(node->left) == visited.end()) {
                    Q.push_back(node->left);
                } 
                if (node->right && visited.find(node->right) == visited.end()) {
                    Q.push_back(node->right);
                }
                // handle this node
                if (node->left == NULL && node->right == NULL) {
                    sum += getNumber(Q);
                }
                Q.pop_back();
                visited.insert(make_pair(node, true));
            }
            return sum;
        }

        int getNumber(vector<TreeNode*> const& vec) {
            cout << "#######" << endl;
            for (size_t i = 0; i < vec.size(); i++) {
                cout << vec[i]->val << ",";
            }
            cout << "\b" << endl;
            size_t i = 0;
            while (vec[i] == 0) {
                i++;
            }
            int sum = 0;
            for (; i < vec.size(); i++) {
                sum = 10 * sum + vec[i]->val;
            }
            return sum;
        }
};
