#include <iostream>
#include <vector>
#include <numeric>
#include <functional>

using namespace std;

class Solution {
    public:
        vector<vector<int> > combinationSum2(vector<int> &candidates, int target) 
        {
            vector<vector<int> > result;
            vector<int> solution;
            sort(candidates.begin(), candidates.end());
            int sum = 0;
            combinationSum2Sub(candidates, sum,  target, 0,  result, solution);
            return result;
        }

        void combinationSum2Sub(
                vector<int> &candidates, 
                int &sum,
                int target,
                int start, 
                vector<vector<int> > &result, 
                vector<int> &solution
                )
        {
            for (int i = start; i < candidates.size(); i++) {

                if (sum > target) { 
                    return;
                }

                solution.push_back(candidates[i]);
                sum += candidates[i];
                if (sum == target) {
                    for (int j = 0; j < solution.size(); j++) {
                        cout << solution[j] << " ";
                    }
                    cout << endl;
                    result.push_back(solution);
                }
                combinationSum2Sub(candidates, sum, target, i + 1, result, solution);
                solution.pop_back();
                sum -= candidates[i];

                while (i + 1 < candidates.size() && candidates[i] == candidates[i+1]) {
                    i++;
                }
            }
        }
};


int main()
{
    Solution solution;
    vector<int> candidates;
    candidates.push_back(10);
    candidates.push_back(1);
    candidates.push_back(2);
    candidates.push_back(7);
    candidates.push_back(6);
    candidates.push_back(1);
    candidates.push_back(5);
    solution.combinationSum2(candidates, 8);
    return 0;
}
