#ifndef __REVERSEINTEGERTEST_H
#define __REVERSEINTEGERTEST_H

#include <cxxtest/TestSuite.h>
namespace ReverseIntegerWrapper {
#include "ReverseInteger.h"
}

class ReverseIntegerTest : public CxxTest::TestSuite {
    private:
        ReverseIntegerWrapper::Solution solution;

    public:
        void setUp() {

        }

        void tearDown() {

        }

        void testReverseInteger() {
            TS_ASSERT_EQUALS(solution.reverse(123), 321);
            TS_ASSERT_EQUALS(solution.reverse(-123), -321);
        }
};

#endif
