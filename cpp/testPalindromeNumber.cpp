#include <iostream>
using namespace std;

bool isPalindrome(int x) {
    if (x < 0) {
        return false;
    } else if (x < 10) {
        return true;
    }
    int xx = x;
    int y = 0;
    while (x > 0) {
        y = y * 10 + x % 10;
        x = x / 10;
        cout << x << "," << y << endl;
    }
    return xx == y;
}

int main() 
{
    cout << isPalindrome(11) << endl;;
    return 0;
}
