#include <iostream>
#include <vector>
#include <string>
using namespace std;

template <typename T>
ostream& operator<<(ostream& os, const vector<T> &elems) {
    os << "[";
    for (auto it = elems.begin(); it != elems.end(); it++) {
        os << *it << ",";
    }
    if (elems.size() == 0) {
        os << "]";
    } else {
        os << "\b]";
    }
    return os;
}
string simplifyPath(string path) {
    vector<string> result;
    int n = path.size();
    string name;
    for (int i = 0; i <= n; i++) {
        if (i == n || path[i] == '/') {
            if (name == "") {
            } else if (name == ".") {
                name = "";
            } else if (name == "..") {
                if (!result.empty()) { 
                    result.pop_back();
                }
                name = "";
            } else {
                result.push_back(name);
                name = "";
            }
        } else {
            name += path[i];
        }
    }
    string s = "";
    for (int i = 0; i < result.size(); i++) {
        s += '/';
        s += result[i];
    }
    return s == "" ? "/" : s;
}


int main(int argc, const char *argv[])
{
    string s;

    s = "/home/";
    cout << s << " ==> " << simplifyPath(s) << endl;

    s = "/a/./b/../../c/";
    cout << s << " ==> " << simplifyPath(s) << endl;

    s = "//";
    cout << s << " ==> " << simplifyPath(s) << endl;

    s = "/home///";
    cout << s << " ==> " << simplifyPath(s) << endl;

    s = "";
    cout << s << " ==> " << simplifyPath(s) << endl;

    s = "/../";
    cout << s << " ==> " << simplifyPath(s) << endl;

    s = "/...";
    cout << s << " ==> " << simplifyPath(s) << endl;

    s = "/.";
    cout << s << " ==> " << simplifyPath(s) << endl;

    s = "/home/../../../";
    cout << s << " ==> " << simplifyPath(s) << endl;

    s = "/home/../../..";
    cout << s << " ==> " << simplifyPath(s) << endl;

    s = "/home/../../";
    cout << s << " ==> " << simplifyPath(s) << endl;

    return 0;
}
