/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
    private:
        TreeNode *first;
        TreeNode *second;
        TreeNode *last;

    public:
        void recoverTree(TreeNode *root) {
            first = second = NULL;
            last = new TreeNode(INT_MIN);
            traverse(root);
            if (first && second) {
                swap(first->val, second->val);
            } 
        }

        void traverse(TreeNode *root) {
            if (root == NULL) {
                return;
            }
            traverse(root->left);
            if (last->val > root->val) {
                if (first == NULL) {
                    first = last;
                }
                second = root;
            }
            last = root;
            traverse(root->right);
        }
};
