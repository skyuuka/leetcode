#include <iostream>
#include <vector>
#include <string>
using namespace std;

void helper(vector<string> &result, string s, int left, int right) {
    cout << s;
    if (left < 0 || right < 0 || left > right) {
        return;

    }
    if (left == 0 && right == 0) {
        cout << "--> ok" << endl;
        result.push_back(s);
        return;
    }
    cout << endl;
    helper(result, s + "(", left - 1, right);
    helper(result, s + ")", left, right - 1);
}

vector<string> generateParenthesis(int n) {
    vector<string> result;
    if (n <= 0) {
        return result;
    }
    string s = "";
    helper(result, s, n, n);
    return result;
}

int main() {
    generateParenthesis(3);
    return 0;
}
