#include <iostream>
#include <cstdlib>
#include <vector>
#include <cassert>
#include <queue>
#include <string>
#include <sstream>
#include "LeetCodeUtils.h"

using namespace std;

void leetcode::freeTree(leetcode::TreeNode *&root) {
    if (root) {
        freeTree(root->left);
        freeTree(root->right);
        delete root;
        root = NULL;
    }
}

bool leetcode::isSameTree(const leetcode::TreeNode* p, const leetcode::TreeNode* q) {
    if (p == NULL && q == NULL) {
        return true;
    }

    if (p == NULL || q == NULL) {
        return false;
    }

    if (p->val != q->val) {
        return false;
    }

    return isSameTree(p->left, q->left) && isSameTree(p->right, q->right);
}

bool leetcode::isValidTreeString(const string& s) {
    if (s.length() >= 2) {
        if (s[0] == '{' && s[s.length() - 1] == '}') {
            return true;
        }
    }
    return false;
}

vector<string>& leetcode::split(const string& s, char delim, vector<string> &elems) {
    stringstream ss(s);
    string item;
    while (getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}

vector<string> leetcode::split(const string& s, char delim) {
    vector<string> elems;
    split(s, delim, elems);
    return elems;
}


/** 
 * @brief load Tree from string
 *
 * The serialization of a binary tree follows a level 
 * order traversal, where '#' signifies a path terminator
 * where no node exists below.
 */
leetcode::TreeNode* leetcode::convertStringToTree(const string& s) {
    if (isValidTreeString(s) == false) {
        return NULL;
    }

    vector<string> elems = split(s.substr(1, s.length() - 2), ',');

    if (elems.size() == 0) {
        return NULL;
    }
    if (elems[0] == "#") {
        return NULL;
    }
    leetcode::TreeNode *root = new leetcode::TreeNode(stoi(elems[0]));
    queue<leetcode::TreeNode*> Q;
    Q.push(root);

    size_t position = 1;
    while (!Q.empty() && position < elems.size()) {
        int size = Q.size();
        for (int i = 0; i < size; i++) {
            leetcode::TreeNode *node = Q.front();
            Q.pop();
            if (elems[position] == "#") {
                node->left = NULL;
            } else {
                leetcode::TreeNode *left = new leetcode::TreeNode(stoi(elems[position]));
                node->left = left;
                Q.push(left);
            }
            position++;
            if (elems[position] == "#") {
                node->right = NULL;
            } else {
                leetcode::TreeNode *right = new leetcode::TreeNode(stoi(elems[position]));
                node->right = right;
                Q.push(right);
            }
            position++;
        }
    }
    return root;
}

leetcode::TreeNode* leetcode::loadTreeFromFile(const string& filename) {
    assert(filename.length() > 0);
    return NULL;
}

string leetcode::convertTreeToString(leetcode::TreeNode *root) {
    if (root == NULL) {
        return "{}";
    }
    string s = "{";
    queue<leetcode::TreeNode*> Q;
    Q.push(root);

    while (!Q.empty()) {
        int size = Q.size();
        for (int i = 0; i < size; i++) {
            leetcode::TreeNode *node = Q.front();
            Q.pop();
            if (node) {
                s += to_string(node->val);
            } else {
                s += "#";
            }
            s += ",";

            if (node) {
                Q.push(node->left);
                Q.push(node->right);
            } 
        }
    }
    // remove pair of #'s
    while (s.length() >= 2 * 2) {
        if (s.substr(s.length() - 2 * 2, 4) == "#,#,") {
            s = s.substr(0, s.length() - 4);
        } else {
            break;
        }
    }
    s[s.length() - 1] = '}';
    return s;
}

void leetcode::saveTreeToFile(const string& filename, leetcode::TreeNode *root) {
    root = NULL;
    assert(filename.length() > 0);
}


void leetcode::freeList(ListNode *head) {
    ListNode *p;
    while (head) {
        p = head;
        head = head->next;
        delete p;
    }
}

bool leetcode::isSameList(const leetcode::ListNode *p, const leetcode::ListNode *q) {
    leetcode::ListNode *l1 = const_cast<leetcode::ListNode*>(p);
    leetcode::ListNode *l2 = const_cast<leetcode::ListNode*>(q);
    while (l1 && l2) {
        if (l1->val != l2->val) {
            return false;
        }
        l1 = l1->next;
        l2 = l2->next;
    }
    return l1 == NULL && l2 == NULL;
}

ostream& leetcode::operator<<(ostream& os, leetcode::ListNode *head) {
    os << "[";
    leetcode::ListNode *cur = head;
    while (cur) {
        os << cur->val << "->";
        cur = cur->next;
    }
    os << "NULL";
    return os;
}
