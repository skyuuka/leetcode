#ifndef _ADDBINARYTEST_H
#define _ADDBINARYTEST_H

#include <cxxtest/TestSuite.h>
namespace AddBinaryWrapper {
#include <string>
using namespace std;
#include "AddBinary.h"
}

class AddBinaryTest: public CxxTest::TestSuite {
    private:
        AddBinaryWrapper::Solution solution;

    public:
        void setUp() {

        }

        void tearDown() {

        }

        void testAddBinary() {
            TS_ASSERT_EQUALS(solution.addBinary("11", "1"), "100");
        }
};

#endif
