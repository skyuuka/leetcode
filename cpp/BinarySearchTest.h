#ifndef __BINARYSEARCH_H
#define __BINARYSEARCH_H

#include <cxxtest/TestSuite.h>
namespace BinarySearchWrapper {
#include "BinarySearch.h"
}

class BinarySearchTest: public CxxTest::TestSuite {
    private:
        BinarySearchWrapper::Solution solution;

    public:
        void setUp() {

        }

        void tearDown() {

        }

        void testBinarySearch() {
            {
                int A[] = {1, 2, 3, 3, 4, 5, 10};
                TS_ASSERT_EQUALS(solution.binarySearch(A, 7, 3), 2); 
            }
            {
                int A[] = {1, 1, 1};
                TS_ASSERT_EQUALS(solution.binarySearch(A, 3, 1), 0); 
            }
        }
};


#endif
