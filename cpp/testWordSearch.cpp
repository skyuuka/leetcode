#include <vector>
#include <iostream>
#include "LeetCodeUtils.h"
using namespace std;
using namespace leetcode;

bool dfs(vector<vector<char> > &board, int i, int j,  string &word, int start) {
    if (start == word.size()) {
        return true;
    }
    if (i < 0 || i >= board.size() || j < 0 || j >= board[0].size() || word[start] != board[i][j]) {
        return false;
    }
    board[i][j] = '#';
    bool rst = 
        dfs(board, i - 1, j,  word, start + 1) | 
        dfs(board, i + 1, j,  word, start + 1) | 
        dfs(board, i, j - 1,  word, start + 1) | 
        dfs(board, i, j + 1,  word, start + 1); 
    board[i][j] = word[start];
    return rst;
}
bool exist(vector<vector<char> > &board, string word) {
    if (board.size() == 0 || board[0].size() == 0) {
        return false;
    }
    for (int i = 0; i < board.size(); i++) {
        for (int j = 0; j < board[i].size(); j++) {
            if (word[0] == board[i][j] && dfs(board, i, j, word, 0))  return true;
        }
    }   
    return false;
}

int main(int argc, const char *argv[])
{
    vector<vector<char> > board(3, vector<char>(4));
    board[0][0] = 'A'; 
    board[0][1] = 'B'; 
    board[0][2] = 'C'; 
    board[0][3] = 'E'; 
    board[1][0] = 'S'; 
    board[1][1] = 'F'; 
    board[1][2] = 'C'; 
    board[1][3] = 'S'; 
    board[2][0] = 'A'; 
    board[2][1] = 'D'; 
    board[2][2] = 'E'; 
    board[2][3] = 'E'; 
    cout << board << endl;
    vector<vector<char> > board_ = board;
    {
        string word = "ABCCED";
        cout << word << " ==> " << exist(board, word) << endl;
    }
    {
        string word = "SEE";
        cout << word << " ==> " << exist(board, word) << endl;
    }
    {
        string word = "ABCB";
        cout << word << " ==> " << exist(board, word) << endl;
    }

    return 0;
}
