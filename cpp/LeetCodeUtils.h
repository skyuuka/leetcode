#ifndef __LEETCODEUTILS_H
#define __LEETCODEUTILS_H

#include <vector>
#include <string>

namespace leetcode {
    using namespace std;
    /// TreeNode
    struct TreeNode {
        int val;
        TreeNode *left;
        TreeNode *right;
        TreeNode(int x) : val(x), left(NULL), right(NULL) {}
    };


    void freeTree(TreeNode *&root);
    bool isSameTree(const TreeNode* p, const TreeNode* q);
    bool isValidTreeString(const string& str);

    vector<string>& split(const string& s, char delim, vector<string> &elems);

    vector<string> split(const string& s, char delim);

    template<typename T>
        ostream& operator<<(ostream& os, vector<T> const& elems) {
            os << "[";
            for (auto it = elems.begin(); it != elems.end(); it++) {
                os << *it << ",";
            }
            if (elems.size() == 0) {
                os << "]";
            } else {
                os << "\b]";
            }
            return os;
        }

    /** 
     * @brief load Tree from string
     *
     * The serialization of a binary tree follows a level 
     * order traversal, where '#' signifies a path terminator
     * where no node exists below.
     */
    TreeNode* convertStringToTree(const string& s);
    TreeNode* loadTreeFromFile(const string& filename);
    string convertTreeToString(TreeNode *root);
    void saveTreeToFile(const string& filename, TreeNode *root);


    struct ListNode {
        int val;
        ListNode *next;
        ListNode(int x) : val(x), next(NULL) {}
    };

    ostream& operator<<(ostream& os, ListNode *head);

    void freeList(ListNode *head);
    bool isSameList(const ListNode *p, const ListNode *q);
}
#endif
