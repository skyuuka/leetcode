#ifndef __SORTLIST_H
#define __SORTLIST_H

#include <cxxtest/TestSuite.h>
#include "LeetCodeUtils.h"

using namespace leetcode;

namespace SortListWrapper {
#include <cstdlib>
#include "SortList.h"
}


class SortListTest: public CxxTest::TestSuite {
    private:
        vector<vector<ListNode*> > tests;
        SortListWrapper::Solution solution;

    public:
        void setUp() {
            // root, p, q, correct
            vector<ListNode*> test(2, NULL);
            ListNode *p;

            // null
            tests.push_back(test);

            // single node
            test[0] = new ListNode(5);
            test[1] = new ListNode(5);
            tests.push_back(test);

            // 2 elems
            test[0] = new ListNode(5); 
            test[0]->next = new ListNode(4);

            test[1] = new ListNode(4);
            test[1]->next = new ListNode(5);
            tests.push_back(test);

            //
            test[0] = new ListNode(5); p = test[0];
            p->next = new ListNode(4); p = p->next;
            p->next = new ListNode(3); p = p->next;
            p->next = new ListNode(2); p = p->next;
            p->next = new ListNode(1); p = p->next;

            test[1] = new ListNode(1); p = test[1];
            p->next = new ListNode(2); p = p->next;
            p->next = new ListNode(3); p = p->next;
            p->next = new ListNode(4); p = p->next;
            p->next = new ListNode(5); p = p->next;
            tests.push_back(test);
        }

        void tearDown() {
            for (size_t i = 0; i < tests.size(); i++) {
                    freeList(tests[i][0]);
                    freeList(tests[i][1]);
                    tests[i].clear();
            }
            tests.clear();
        }

        void testSortList() 
        {
            for (size_t i = 0; i < tests.size(); i++) {
                ListNode *p = tests[i][0];
                ListNode *correct = tests[i][1];
                ListNode *result = solution.sortList(p);
                TSM_ASSERT("Case #" + to_string(i + 1) + ":", 
                        isSameList(result, correct)
                        );
            }
        }
};

#endif
