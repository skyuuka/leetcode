/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode *reverseBetween(ListNode *head, int m, int n) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        ListNode headptr(0);
        headptr.next = head;
        ListNode *cur, *ins, *move;
        ins = &headptr;
        cur = head;
        for (int i = 1; i < n; i++) {
            if (i<m) {
                cur = cur->next;
                ins = ins->next;
            } else if (i>=m && i<n)  {
                move = cur->next;
                cur->next = move->next;
                move->next = ins->next;
                ins->next = move;
            }
        }
        return headptr.next;
    }
};
