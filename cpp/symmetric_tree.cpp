/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    bool isSymmetric(TreeNode *root) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        queue<TreeNode*> Q1, Q2;

        Q1.push(root);
        Q2.push(root);
            TreeNode *cur1 = Q1.front();
            TreeNode *cur2 = Q2.front();
            
            if (cur1->val != cur2->val) return false;
            
            if (cur1->left) {
                Q1.push(cur1->left);
                Q2.push(cur2->right);
            } else {
                if (cur2->right) return false;
            }
            
            if (cur1->right) {
                Q1.push(cur1->right);
                Q2.push(cur2->left);
            } else {
                if (cur2->left) return false;
            }
            
            Q1.pop();
            Q2.pop();
        }
        return Q1.empty() && Q2.empty();
    }
};
