#include <iostream>
#include <queue>
#include <vector>
using namespace std;

void printBoard(vector<vector<char> > const& board) {
    for (int i = 0; i < board.size(); i++) {
        for (int j = 0; j < board[i].size(); j++) {
            cout << board[i][j] << " ";
        }
        cout <<"\b" << endl;
    }
}

struct Node {
    int x;
    int y;
    Node(int x, int y) : x(x), y(y) {}
    Node(const Node &other) {
        this->x = other.x;
        this->y = other.y;
    }
};

void changeRegion(vector<vector<char> > &board, int x, int y, char fromval, char toval) {
    size_t m = board.size();
    if (m == 0) {
        return;
    }       

    if (board[y][x] != fromval) {
        return;
    }

    size_t n = board[0].size();

    queue<Node> Q;
    if (board[y][x] == fromval) {
        board[y][x] = toval;
        Q.push(Node(x, y));

    }
    while (Q.empty()) {
        size_t size = Q.size();
        for (size_t i = 0; i < size; i++) {
            Node node = Q.front();
            Q.pop();
            int xx = node.x;
            int yy = node.y;
            if (xx - 1 >= 0 && board[yy][xx - 1] == fromval) {
                board[xx - 1][yy] = toval;
                Q.push(Node(xx - 1, yy));
            }
            if (xx + 1 < n && board[yy][xx + 1] == fromval) {
                board[xx + 1][yy] = toval;
                Q.push(Node(xx + 1, yy));
            }
            if (yy - 1 >= 0 && board[yy - 1][xx] == fromval) {
                board[xx][yy - 1] = toval;
                Q.push(Node(xx, yy - 1));
            }
            if (yy + 1 < m && board[yy + 1][xx] == fromval) {
                board[xx][yy + 1] = toval;
                Q.push(Node(xx, yy + 1));
            }
        }
    }
}

void solve(vector<vector<char> > &board) {
    size_t m = board.size();
    if (m == 0) {
        return;
    }
    size_t n = board[0].size();

    for (int x = 0; x < n; x++) {
        changeRegion(board, x, 0, '0', 'Y');
        changeRegion(board, x, m - 1, '0', 'Y');
    }

    for (int y = 0; y < m; y++) {
        changeRegion(board, 0, y, '0', 'Y');
        changeRegion(board, n - 1, y, '0', 'Y');
    }

    for (int y = 1; y < m - 1; y++) {
        for (int x = 1; x < n - 1; x++) {
            if (board[y][x] == 'X') {
                continue;
            }
            changeRegion(board, x, y, '0', 'X');
        }
    }

    for (int x = 0; x < n; x++) {
        changeRegion(board, x, 0, 'Y', '0');
        changeRegion(board, x, m - 1, 'Y', '0');
    }

    for (int y = 0; y < m; y++) {
        changeRegion(board, 0, y, 'Y', '0');
        changeRegion(board, n - 1, y, 'Y', '0');
    }
}


int main() {
    vector<vector<char> > board(3, vector<char>(3, 'X'));
    board[1][1] = '0';

    cout << "old:" << endl;
    printBoard(board);
    solve(board);
    cout << "new:" << endl;
    printBoard(board);
}
