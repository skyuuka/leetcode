#ifndef __FLATTENBINARYTREETEST_H
#define __FLATTENBINARYTREETEST_H

#include <cxxtest/TestSuite.h>
#include "LeetCodeUtils.h"

using namespace leetcode;

namespace FlattenBinaryTreeWrapper {
#include "FlattenBinaryTree.h"
}


class FlattenBinaryTreeTest: public CxxTest::TestSuite {
    private:
        vector<pair<TreeNode*, TreeNode*> > tests;
        FlattenBinaryTreeWrapper::Solution solution;

    public:
        void setUp() {
            tests.push_back(
                    make_pair<TreeNode*, TreeNode*>(
                        convertStringToTree("{}"),
                        convertStringToTree("{}")
                        )
                    );

            tests.push_back(
                    make_pair<TreeNode*, TreeNode*>(
                        convertStringToTree("{1}"), 
                        convertStringToTree("{1}") 
                        )
                    );

            tests.push_back(
                    make_pair<TreeNode*, TreeNode*>(
                        convertStringToTree("{1,2,5,3,4,#,6}"), 
                        convertStringToTree("{1,#,2,#,3,#,4,#,5,#,6}") 
                        )
                    );

            tests.push_back(
                    make_pair<TreeNode*, TreeNode*>(
                        convertStringToTree("{1,2,3}"), 
                        convertStringToTree("{1,#,2,#,3}") 
                        )
                    );

            tests.push_back(
                    make_pair<TreeNode*, TreeNode*>(
                        convertStringToTree("{1,#,2,#,3}"), 
                        convertStringToTree("{1,#,2,#,3}") 
                        )
                    );
        }

        void tearDown() {
            for (size_t i = 0; i < tests.size(); i++) {
                freeTree(tests[i].first);
                freeTree(tests[i].second);
            }
            tests.clear();
        }

        void testConstructTree() 
        {
            TS_TRACE("Starting flatten binary tree");
            for (size_t i = 0; i < tests.size(); i++) {
                TreeNode *p = tests[i].first;
                TreeNode *q = tests[i].second;
                solution.flatten(p);
                TSM_ASSERT("Case #" + to_string(i + 1) + ":", 
                        isSameTree(p, q)
                        );
            }
            TS_TRACE("Finishing flatten binary tree");
        }
};

#endif
