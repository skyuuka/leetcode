#include <iostream>
#include <string>
using namespace std;
/*
 * state: f[i][j] isMatch(s[0 : i], p[0 : j])
 * function: f[i][j] =      f[i][j - 1] && p[j] == '*'
 *                      ||  f[i - 1][j] && p[j] == '*'
 *                      ||  f[i - 1][j - 1] &&  s[i] == p[j] || p[j] == '*' || p[j] == '?'
 * initialize:
 * answer: f[n - 1][m - 1]
 */
bool isMatch(const char *s, const char *p) {
    if (s == NULL || p == NULL) {
        return false;
    }
    if (strlen(p) == 0) {
        return strlen(s);
    }
    int n = strlen(s);
    int m = strlen(p);
    bool f[n][m];
    f[0][0] = s[0] == p[0] || p[0] == '*' || p[0] == '?';
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            if (i == 0 && j == 0) {
                continue;
            }
            bool  r = (
                    p[j] == '*' && 
                    (
                     (j - 1 >= 0 && f[i][j - 1]) ||
                     (i - 1 >= 0 && f[i -1][j])  ||
                     (i - 1 >= 0 && j - 1 >= 0 && f[i - 1][j - 1])
                    )
                    )
                ||
                (
                 (p[j] == '?' || p[j] == s[i]) && (i - 1 >= 0 && j - 1 >= 0 && f[i - 1][j - 1])
                );
            f[i][j] = r;
        }
    }
    return f[n - 1][m - 1];
}
int main(int argc, const char *argv[])
{
    char *s, *p;
    s = "aaabababaaabaababbbaaaabbbbbbabbbbabbbabbaabbababab";
    p = "*ab***ba**b*b*aaab*b";
    cout << s << " : " << p << " : " << isMatch(s, p)  << endl;
    s = "aa";
    p = "a";
    cout << s << " : " << p << " : " << isMatch(s, p)  << endl;
    s = "aa";
    p = "aa";
    cout << s << " : " << p << " : " << isMatch(s, p)  << endl;
    s = "aaa";
    p = "aa";
    cout << s << " : " << p << " : " << isMatch(s, p)  << endl;
    s = "aa";
    p = "*";
    cout << s << " : " << p << " : " << isMatch(s, p)  << endl;
    s = "aa";
    p = "a*";
    cout << s << " : " << p << " : " << isMatch(s, p)  << endl;
    s = "ab";
    p = "?*";
    cout << s << " : " << p << " : " << isMatch(s, p)  << endl;
    s = "aab";
    p = "c*a*b";
    cout << s << " : " << p << " : " << isMatch(s, p)  << endl;
    return 0;
}
