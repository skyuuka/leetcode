class Solution {

    void printVec(const vector<int> &vec) {
        cout << "[";
        for (size_t i = 0; i < vec.size(); i++) {
            cout  << vec[i] << "," ; 
        }
        cout << "\b]";
    }

    public:
    vector<vector<string> > solveNQueens(int n) {
        vector<vector<string> > result;
        if (n <= 0) {
            return result;
        }
        vector<int> cols;
        // to fill n value in cols
        search(n, cols, result);
        return result;
    }

    vector<string> drawChessboard(vector<int> const& cols) {
        int n = cols.size();
        vector<string> chessboard(n, string(n, '.'));
        for (int i = 0; i < n; i++) {
            int col = cols[i];
            chessboard[i][col] = 'Q';
        }
        return chessboard;
    }
    /**
     * place the row starts from __start__ (0, ..., n-1)
     */
    void search(int n, vector<int> &cols, vector<vector<string> > &result) {
        if (static_cast<int>(cols.size()) == n) {
            result.push_back(drawChessboard(cols));
        } 

        // fill the rest positions
        for (int col = 0; col < n; col++) {//try 0 to n - 1
            if (can(col, cols)) {
                cols.push_back(col);
                search(n, cols, result);
                cols.pop_back();
            }
        }
    }

    bool can(int col, vector<int> const& cols) {
        int row = cols.size();
        for (int i = 0; i < row; i++) {
            /*
               if (cols[i] == col || abs(col - cols[i]) == row - i) {
               return false;
               }
               */
            // same column
            if (cols[i] == col) {
                return false;
            }
            // '\'
            if (col - cols[i] == row - i) {
                return false;
            }
            // '/'
            if (cols[i] - col == row - i) {
                return false;
            }
        }
        return true;
    }
};
