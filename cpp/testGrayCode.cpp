#include <vector>
#include <iostream>
#include "LeetCodeUtils.h"
using namespace std;
using namespace leetcode;

vector<int> grayCode(int n) {
    vector<int> codes;
    if (n < 0) {
        return codes;
    }
    codes.push_back(0);
    for (int i = 0; i < n; i++) {
       int size = codes.size();
       long long int high = (1 << i);
       for (int j = size - 1; j >= 0; j--) {
           codes.push_back(codes[j] + high);
       }
    }
    return codes;
}

int main(int argc, const char *argv[])
{
    for (int i = 0; i < 10; i++) {
        cout << i << " ==> " << grayCode(i) << endl;
    }
    return 0;
}
