#include <iostream>
#include <cstdlib>
using namespace std;

struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

ListNode *reverseKGroup(ListNode *head, int k) {
    if (head == NULL) {
        return NULL;

    }
    if (k <= 1) {
        return head;
    }

    ListNode *dummy = new ListNode(0);
    dummy->next = head;

    ListNode *cur = head, *pre = dummy, *tail = NULL;
    while (cur) {
        cout << "#" << cur->val << ",";
        tail = cur;
        cur = cur->next;
        tail->next = NULL;
        int i = 1;
        for (; i < k && cur; i++) {
            ListNode *next = cur->next;
            cur->next = pre->next;
            pre->next = cur;
            cur = next;
        }

        if (cur == NULL) {
            break;
        }

        tail->next = cur;
        pre = tail;
    }
    cout << endl;

    head = dummy->next;
    delete dummy;
    return head;
}

int main() {
    ListNode *head = new ListNode(1);
    head->next = new ListNode(2);
    head = reverseKGroup(head, 2);
    ListNode *cur = head;
    while (cur) {
        cout << cur->val << ",";
        cur = cur->next;
    }
    cout << "\b" << endl;
    return 0;
}
