#include <iostream>
#include <vector>
#include <numeric>
#include <functional>

using namespace std;

class Solution {
    public:
        vector<vector<int> > combinationSum(vector<int> &candidates, int target) 
        {
            vector<vector<int> > result;
            vector<int> solution;
            sort(candidates.begin(), candidates.end());
            combinationSumSub(candidates, target, 0, result, solution);
            return result;
        }

        void combinationSumSub(
                vector<int> &candidates, 
                int target,
                int start, 
                vector<vector<int> > &result, 
                vector<int> &solution
                )
        {
            for (int i = start; i < candidates.size(); i++) {
                int repeat = 0;
                while (accumulate(solution.begin(), solution.end(), 0) < target) {
                    repeat++;
                    solution.push_back(candidates[i]);
                    if (accumulate(solution.begin(), solution.end(), 0) == target) {
                        for (int j = 0; j < solution.size(); j++) {
                            cout << solution[j] << " ";
                        }
                        cout << endl;
                        result.push_back(solution);
                    }
                    combinationSumSub(candidates, target, i + 1, result, solution);
                }
                while (repeat--) {
                    solution.pop_back();
                }
            }
        }
};


int main()
{
    Solution solution;
    vector<int> candidates;
    candidates.push_back(8);
    candidates.push_back(7);
    candidates.push_back(4);
    candidates.push_back(3);
    solution.combinationSum(candidates, 11);
    return 0;
}
