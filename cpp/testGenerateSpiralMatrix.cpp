#include <iostream>
#include <ostream>
#include <vector>
using namespace std;

template <typename T>
ostream& operator<<(ostream& os, const vector<T> &elems) {
    os << "[";
    for (auto it = elems.begin(); it != elems.end(); it++) {
        os << *it << ",";
    }
    if (elems.size() == 0) {
        os << "]";
    } else {
        os << "\b]";
    }
    return os;
}
vector<vector<int> > generateMatrix(int n) {
    vector<vector<int> > matrix(n, vector<int>(n));
    int val = 1;
    for (int k = 0; k < n / 2; k++) {
        int row = k;
        int col = k;
        int size = n - 2 * k;
        // top
        for (int i = 0; i < size; i++) {
            matrix[row][col + i] = val++;
        }
        // right
        for (int i = 1; i < size; i++) {
            matrix[row + i][col + size - 1] = val++;  
        }
        // bottom
        for (int i = size - 2; i >= 0; i--) {
            matrix[row + size - 1][col + i] = val++;
        }
        // left
        for (int i = size - 2; i >= 1; i--) {
            matrix[row + i][col] = val++;
        }
    }
    if (n % 2 == 1) {
        matrix[n / 2][n / 2] = val;
    }
    return matrix;
}

int main(int argc, const char *argv[])
{
    for (int n = 0; n < 5; n++) {
        cout << generateMatrix(n) << endl;
    }
    return 0;
}
