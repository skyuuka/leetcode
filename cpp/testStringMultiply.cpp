#include <string>
#include <iostream>
using namespace std;
string add(string num1, string num2) {
    string s(max(num1.size(), num2.size()) + 1, '0');
    int overflow = 0;
    for (int i = 0; i < max(num1.size(), num2.size()); i++) {
        int n1 = i < num1.size() ? num1[num1.size() - 1 - i] - '0' : 0;
        int n2 = i < num2.size() ? num2[num2.size() - 1 - i] - '0' : 0;
        int sum = n1 + n2 + overflow;
        s[s.size() - 1 - i] = sum % 10 + '0';
        overflow = sum / 10;
    }
    s[0] = overflow + '0';
    if (s[0] == '0') {
        s.erase(s.begin());
    }
    return s;
}
string multiply(string num1, string num2) {
    string result;
    for (int i = num2.size() - 1; i >= 0; i--) {
        int n2 = num2[i] - '0';
        string mul(num1.size() + 1, '0');
        int overflow = 0;
        for (int j = num1.size() - 1; j >= 0; j--) {
            int n1 = num1[j] - '0';
            int tmp = n2 * n1 + overflow;
            overflow = tmp / 10;
            int current = tmp % 10;
            mul[j + 1] = current + '0';
        }
        mul[0] = overflow + '0';
        if (mul[0]  == '0') {
            mul.erase(mul.begin());
        }
        mul.insert(mul.end(), num2.size() - 1 - i, '0');
        result = add(result, mul);
    }
    while (result.size() > 1 && result[0] == '0') {
        result.erase(result.begin());
    }
    return result;
}
int main(int argc, const char *argv[])
{
    string s1, s2;
    s1 = "111"; s2 = "111";
    cout << s1 << "+" << s2 << "=" << add(s1, s2) << endl;
    cout << s1 << "*" << s2 << "=" << multiply(s1, s2) << endl;
    s1 = ""; s2 = "999";
    cout << s1 << "+" << s2 << "=" << add(s1, s2) << endl;
    cout << s1 << "*" << s2 << "=" << multiply(s1, s2) << endl;
    s1 = "111"; s2 = "";
    cout << s1 << "+" << s2 << "=" << add(s1, s2) << endl;
    cout << s1 << "*" << s2 << "=" << multiply(s1, s2) << endl;
    s1 = "111"; s2 = "999";
    cout << s1 << "+" << s2 << "=" << add(s1, s2) << endl;
    cout << s1 << "*" << s2 << "=" << multiply(s1, s2) << endl;
    s1 = "111"; s2 = "99";
    cout << s1 << "+" << s2 << "=" << add(s1, s2) << endl;
    cout << s1 << "*" << s2 << "=" << multiply(s1, s2) << endl;
    s1 = ""; s2 = "";
    cout << s1 << "+" << s2 << "=" << add(s1, s2) << endl;
    cout << s1 << "*" << s2 << "=" << multiply(s1, s2) << endl;
    s1 = "999"; s2 = "9999";
    cout << s1 << "+" << s2 << "=" << add(s1, s2) << endl;
    cout << s1 << "*" << s2 << "=" << multiply(s1, s2) << endl;
    s1 = "9"; s2 = "9";
    cout << s1 << "+" << s2 << "=" << add(s1, s2) << endl;
    cout << s1 << "*" << s2 << "=" << multiply(s1, s2) << endl;
    return 0;
}
