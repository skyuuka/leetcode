#include <iostream>
#include <string>
using namespace std;


/*
 * state: f[i][j][n] means isScramble(s1[i: i+n], s2[j: j+n])
 * function: f[i][j][n] = f[i][j][k] && f[i+k][j+k][n - k] ||
 *                      f[i][j+n-k][k] && f[i+k][j][n-k]
 * initialzie: 
 * result: f[0][0][n-1]
 */
bool isScramble(string s1, string s2) {
    if (s1.size() != s2.size()) {
        return false;
    }
    if (s1.size() == 0 || s1 == s2) {
        return true;
    }
    int n = s1.size();
    bool f[n][n][n];
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            f[i][j][0] = s1[i] == s2[j];
        }
    }

    for (int len = 2; len <= n; len++) {
        for (int i = n - len; i >= 0; i--) {
            for (int j = n - len; j >= 0; j--) {
                int k = 1;
                for (; k < len; k++) {
                    bool r1 = f[i][j][k - 1] && f[i + k] [j + k][len - k - 1];
                    bool r2 = f[i][j + len - k][k - 1] && f[i + k][j][len - k - 1];
                    if (r1 || r2) {
                        break;
                    }
                }
                f[i][j][len - 1] = k < len;
            }
        }
    }
    return f[0][0][n - 1];
}

int main(int argc, const char *argv[])
{
    cout << isScramble("rgtae", "great") << endl; 
    cout << isScramble("abc", "cab") << endl; 
    return 0;
}
