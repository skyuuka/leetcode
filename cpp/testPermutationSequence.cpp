#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
using namespace std;
template <typename T>
ostream& operator<<(ostream& os, const vector<T> &elems) {
    os << "[";
    for (auto it = elems.begin(); it != elems.end(); it++) {
        os << *it << ",";
    }
    if (elems.size() == 0) {
        os << "]";
    } else {
        os << "\b]";
    }
    return os;
}

/**
 * increase s[i] by times. 
 * e.g.:
 *  s = "4123"; i = 2; c = 1; ==> s = "4213"
 *  s = "4123"; i = 2; c = 2; ==> s = "4312"
 *  s = "4123"; i = 2; c = 3; ==> invalid 
 *  Note that, s[i..end] is in ascending order, this 
 *  should be guarranteed before calling this function.
 */
void increase(string &s, int i, int c) {
    char cur = s[i];
    s[i] = s[i + c];
    s[i + c] = cur;
    for (int j = c; j >= 2; j--) {
        swap(s[i + j], s[i + j - 1]);
    }
}
string getPermutation(int n, int k) {
    if (n == 0) {
        return "";
    }
    if (n == 1) {
        return "1";
    }
    k--;
    string result;
    for (int i = 1; i <= n; i++) {
        result.push_back(i + '0');
    }
    // Compute the change of orders when each bit increase
    // e.g.,  n = 3 --> count = [2, 1, 0]
    //        n = 4 --> count = [6, 2, 1, 0]
    vector<int> count(n);
    count[n - 1] = 0;
    count[n - 2] = 1;
    for (int i = n - 3; i >= 0; i--) {
        count[i] = count[i + 1] * (n - 1 - i);
    }
    int maxPerm = count[0] * n;
    // Handle the case when k is too large than the number of all possible
    // permutations. In this case, we just restart from the beginning. 
    if (k >= n) {
        k = k % maxPerm;
    }
    for (int i = 0; i < n - 1; i++) {
        // increase i-th bit k/count[i] times
        increase(result, i, k / count[i]);
        k = k % count[i];
    }
    return result;
}
int main(int argc, const char *argv[])
{
    int n = 3;
    for (int i = 1; i <= 24; i++) {
        cout << "(" << n << "," << i << ")" << " -> " <<  getPermutation(n, i) << endl;
    }
    cout << "(1,1)" << " -> " << getPermutation(1, 1) << endl; 
    return 0;
}
