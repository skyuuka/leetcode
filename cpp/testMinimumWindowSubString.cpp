#include <iostream>
#include <vector>
#include <string>
using namespace std;
string minWindow(string S, string T) {
    if (T.size() == 0 || S.size() == 0) {
        return "";
    }
    vector<int> tCounter(256, 0), sCounter(256, 0);
    for (int i = 0; i < T.size(); i++) {
        tCounter[T[i]]++;
    }
    int leftBound = 0;
    int tCount = 0;
    int minLeftBound = 0, minWdith = INT_MAX;
    for (int i = 0; i < S.size(); i++) {
        char c = S[i];
        if (tCounter[c] == 0) {
            continue;
        }
        sCounter[c]++;
        if (sCounter[c] <= tCounter[c]) {
            tCount++;
        }
        if (tCount == T.size()) {
            while (leftBound < S.size()) {
                char ch = S[leftBound];
                if (tCounter[ch] == 0 ) {
                    leftBound++;
                    continue;
                } 
                if (sCounter[ch] > tCounter[ch]) {
                    sCounter[ch]--;
                    leftBound++;
                    continue;
                }
                break;
            }
            if (minWdith > i - leftBound + 1) {
                minLeftBound = leftBound;
                minWdith = i - leftBound + 1;
            }
        }
    }
    return minWdith == INT_MAX ? "" : S.substr(minLeftBound, minWdith);
}
int main(int argc, const char *argv[])
{
    {
        string S = "ADOBECODEBANC";
        string T = "ABC";
        cout << "Input: S = " << S << "; T = " << T << endl;
        cout << "Output: " << minWindow(S, T) << endl;
    }
    {
        string S = "a";
        string T = "aa";
        cout << "Input: S = " << S << "; T = " << T << endl;
        cout << "Output: " << minWindow(S, T) << endl;
    }
    return 0;
}
