#include <iostream>
#include <vector>
using namespace std;

class Solution {
    public:
        vector<vector<int> > permuteUnique(vector<int> &num) {

            vector<vector<int> > result;
            vector<int> visited(num.size(), 0);
            vector<int> solution;

            sort(num.begin(), num.end());
            generatePermuteUnique(num, 0, result, solution, visited);
            return result;

        }

        void generatePermuteUnique(
                vector<int> &num, 
                int start, 
                vector<vector<int> > &result,
                vector<int> &solution,
                vector<int> &visited

                )
        {
            for(int i = 0; i < num.size(); i++) {
                if (visited[i] == 0) {
                    solution.push_back(num[i]);
                    if (solution.size() == num.size()) {
                        result.push_back(solution);
                        for (int j = 0; j < solution.size(); j++)
                            cout << solution[j];
                        cout << endl;
                    }

                    visited[i] = 1;
                    generatePermuteUnique(num, start + 1, result, solution, visited);
                    visited[i] = 0;

                    solution.pop_back();

                    while (i + 1 < num.size() && num[i] == num[i + 1]) {
         //               i++;

                    }


                }

            }

        }

};


int main()
{
    Solution solution;
    vector<int> num;
    num.push_back(1);
    num.push_back(2);
    solution.permuteUnique(num);
    return 0;
}
