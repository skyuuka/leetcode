About
---
C++ implementation for solving [LeetCode](https://oj.leetcode.com/problems/).


How to Run?
---
For some solutions, there is a test written using
[CxxTest](http://cxxtest.com/); while for others, a `main` function has been
provided in the same file. 

* To run the code with test written using [CxxTest](http://cxxtest.com/), a
  `Makefile` is provided to compile and run the tests. 
    - `make` to compile the test suites
    - `make t` to run the tests
    - `make doc` to generate documentation.
      [Doxygen](http://www.stack.nl/~dimitri/doxygen/) required.

* To run the code with `main` function. 
    - compile it:  `g++ -std=c++11 your_file.cpp -o your_program`
    - run it: `./your_program`
