#include <iostream>
#include <vector>
#include "LeetCodeUtils.h"
using namespace std;
using namespace leetcode;
/**
  Definition for singly-linked list.
  struct ListNode {
  int val;
  ListNode *next;
  ListNode(int x) : val(x), next(NULL) {}
  };
  */
ListNode *mergeKLists(vector<ListNode *> &lists) {
    if (lists.size() == 0) {
        return NULL;
    }
    ListNode *dummy = new ListNode(INT_MIN);
    for (int i = 0; i < lists.size(); i++) {
        ListNode *p1 = dummy->next, *pre = dummy;
        ListNode *p2 = lists[i];
        while (p1 && p2) {
            if (p1->val <= p2->val) {
                pre->next = p1;
                pre = p1;
                p1 = p1->next;
            } else {
                pre->next = p2;
                pre = p2;
                p2 = p2->next;
            }
        }
        if (p1) {
            pre->next = p1;
        } else if (p2) {
            pre->next = p2;
        }
    }
    ListNode *head = dummy->next;
    delete dummy;
    return head;
}

int main(int argc, const char *argv[])
{
    vector<ListNode*> lists;
    for (int i = 0; i < 1000; i++) {
        lists.push_back(new ListNode(5));
        lists.push_back(new ListNode(4));
        lists.push_back(new ListNode(1));
        lists.push_back(new ListNode(2));
        lists.push_back(new ListNode(6));
        lists.push_back(new ListNode(1));
        lists.push_back(new ListNode(3));
        lists.push_back(new ListNode(2));
        lists.push_back(new ListNode(8));
    }
    ListNode *head = mergeKLists(lists);
    cout << head << endl;
    return 0;
}
