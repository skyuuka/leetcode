class Solution {
public:
    int numTrees(int n) {
        if (n==0) return 1;
        if (n==1) return 1;
        //if (n==2) return 2;
        
        int fn = 0;
        for (int i = 1; i <= n; i++) {
            fn += (numTrees(i-1) * numTrees(n-i));
        }
        return fn;
    }
};
