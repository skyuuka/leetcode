#include <iostream>
#include <stack>
#include "LeetCodeUtils.h"
using namespace std;
using namespace leetcode;
int evalRPN(vector<string> &tokens) {
    if (tokens.size() == 0) {
        return 0;
    }
    stack<int> S;
    for (int i = 0; i < tokens.size(); i++) {
        string t = tokens[i];
        if (t != "+" && t !=  "-" && t != "*" && t != "/") {
            S.push(stoi(t));
        } else {
            int op1, op2;
            if (!S.empty()) {
                op2 = S.top();
                S.pop();
            } else {
                // invalid experession
            }
            if (!S.empty()) {
                op1 = S.top();
                S.pop();
            } else {
                // invalid experession
            }
            int rst;
            switch (t[0]) {
                case '+':
                    rst = op1 + op2;
                    break;
                case '-':
                    rst = op1 - op2;
                    break;
                case '*':
                    rst = op1 * op2;
                    break;
                case '/':
                    if (op2 == 0) {
                        // invalid experession
                    }
                    rst = op1 / op2;
                    break;
                default:
                    return 0;
                    
            }
            S.push(rst);
        }
    }
    if (S.size() != 1) {
        // invalid experession
    }
    return S.top();
}
int main(int argc, const char *argv[])
{
    const char *s[] =  {"-2", "1", "+", "3", "*"};
    vector<string> tokens(s, s + 5);
    cout << "Input: " << tokens << endl;
    cout << "Output: " <<  evalRPN(tokens) << endl;
    return 0;
}
